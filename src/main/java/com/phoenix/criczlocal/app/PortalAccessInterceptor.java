package com.phoenix.criczlocal.app;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.phoenix.criczlocal.action.PortalAction;
import com.phoenix.criczlocal.constant.ResponseConstant;
import com.phoenix.criczlocal.constant.ResponseStatusCode;
import com.phoenix.criczlocal.db.entity.User;
import com.phoenix.criczlocal.model.web.ErrorResponseBody;
import com.phoenix.criczlocal.model.web.ResponseBody;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * PortalAccessInterceptor
 * @author Sharoon
 */
@Component
public class PortalAccessInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    PortalAction portalAction;

    @Autowired
    Gson gson;

    /**
     * Validating the user is allowed to done operations for portal.
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {

        Map pathVariables = (Map) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);

        System.out.println(pathVariables.toString());
        Long portalId = Long.parseLong(pathVariables.get("portalId").toString());

        User user = portalAction.checkUserPortal(portalId, getRequestUserId());

        if (user == null) {
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            response.setStatus(ResponseStatusCode.NON_AUTHORITATIVE);
            ResponseBody reponseBody = new ResponseBody();
            reponseBody.setStatusCode(ResponseStatusCode.NON_AUTHORITATIVE);
            reponseBody.setIsSuccess(ResponseConstant.FAIL);

            ErrorResponseBody error = new ErrorResponseBody();
            error.setTitle("Access denied.");
            error.setMessage("You are not allowed for this operation.");
            reponseBody.setError(error);

            response.getWriter().print(gson.toJson(reponseBody));
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

    /**
     * Get the logined user id using token.
     * 
     * @return user id
     */
    public Long getRequestUserId() {
        Long userId = null;
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder
                .getContext().getAuthentication().getPrincipal();

        if (user.getUsername() != null) {
            userId = Long.parseLong(user.getUsername());
        }
        return userId;
    }
}