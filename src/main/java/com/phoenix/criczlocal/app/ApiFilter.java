package com.phoenix.criczlocal.app;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.stereotype.Component;

@Component
public class ApiFilter implements Filter {
   @Override
   public void destroy() {}

   @Override
   public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterchain)
           throws IOException, ServletException {

       System.out.println("Remote Host:" + request.getRemoteHost());
       System.out.println("Remote Address:" + request.getRemoteAddr());
       filterchain.doFilter(request, response);
   }

   @Override
   public void init(final FilterConfig filterconfig) throws ServletException {
   }
}