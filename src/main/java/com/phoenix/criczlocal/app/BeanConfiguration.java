package com.phoenix.criczlocal.app;

import java.util.Arrays;
import java.util.List;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import com.google.gson.Gson;

import org.dozer.DozerBeanMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * BeanConfiguration
 * 
 * @author Sharoon
 */
@Configuration
public class BeanConfiguration {

    /**
     * Bean for gson.
     * 
     * @return
     */
    @Bean
    public Gson gson() {
        return new Gson();
    }

    /**
     * Bean for DozerMapper.
     * 
     * @return
     */
    @Bean
    public DozerBeanMapper dozerMapper() {

        List<String> mappingFiles = Arrays.asList("dozer.xml");
        DozerBeanMapper dozerBean = new DozerBeanMapper();
        dozerBean.setMappingFiles(mappingFiles);
        return dozerBean;
    }

    /**
     * Bean for validator.
     * 
     * @return
     */
    @Bean
    public Validator validator() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        return validatorFactory.getValidator();
    }

}