package com.phoenix.criczlocal.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


/**
 * InterceptorAppConfig
 * 
 * @author Sharoon
 */
@Component
public class InterceptorAppConfig extends WebMvcConfigurerAdapter {
    @Autowired
    PortalAccessInterceptor portalAccessInterceptor;
 
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
       registry.addInterceptor(portalAccessInterceptor).addPathPatterns("/portal/**");
    }
 }