package com.phoenix.criczlocal.base;

import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.springframework.beans.factory.annotation.Autowired;

public class GsonBase {

    @Autowired
    Gson gson;

    public static final String SUCCESS = "SUCCESS";

    public <T> T convertJsonToObject(Class<T> convertClass, String jsonObj) {
        return gson.fromJson(jsonObj, convertClass);
    }

    public String toJsonString(Object obj) {
        return gson.toJson(obj);
    }

    public <T> List<T> convertToList(Class<T> cnvertClass, String jsonArr) {
        return gson.fromJson(jsonArr, new TypeToken<List<T>>() {
        }.getType());
    }
}