package com.phoenix.criczlocal.base;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class BeanConversionBase extends GsonBase {

    @Autowired
    DozerBeanMapper dozerMapper;

    /**
     * Creating specified type object from source object.
     * 
     * @param <T>
     * @param <S>
     * @param obj
     * @param convertClass
     * @return T
     */
    public <T, S> T convert(S obj, Class<T> convertClass) {
        return dozerMapper.map(obj, convertClass);
    }

    /**
     * Converting data from Source object to specified destination object.
     * 
     * @param <T>
     * @param <S>
     * @param sourceobj
     * @param destinationobj
     */
    public <T, S> void convert(S sourceobj, T destinationobj) {
        dozerMapper.map(sourceobj, destinationobj);
    }

    /**
     * Creating list from source list to specified type list.
     * 
     * @param <T>
     * @param <S>
     * @param list
     * @param convertClass
     * @return List<T>
     */
    public <T, S> List<T> convertList(List<S> list, Class<T> convertClass) {
        
        List<T> toReturn = new ArrayList<T>();

        if (list == null) {
            return toReturn;
        }

        for (S data : list) {
            toReturn.add(convert(data, convertClass));
        }
        return toReturn;
    }

    /**
     * check null
     * @param obj
     * @return
     */
    public Boolean isNull(Object obj) {
        return obj == null;
    }

    /**
     * Check list null and is empty.
     * @param list
     * @return
     */
    public Boolean isEmpty(List list) {
        if (!isNull(list)) {
            return list.size() > 0 ? Boolean.FALSE : Boolean.TRUE;
        }
        return Boolean.TRUE;
    }
}