package com.phoenix.criczlocal.utils;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.stereotype.Component;

/**
 * ShaUtils
 * 
 * @author Sharoon
 */
@Component
public class ShaUtils {

    private static final Charset UTF_8 = StandardCharsets.UTF_8;
    private static final String OUTPUT_FORMAT = "%-20s:%s";
    private static final String ALGORITHM_SHA_256 = "SHA-256";

    public String encrypt(String str) {
        return bytesToHex(digest(str.getBytes(ShaUtils.UTF_8), ShaUtils.ALGORITHM_SHA_256));
    }

    public byte[] digest(byte[] input, String algorithm) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException(e);
        }
        byte[] result = md.digest(input);
        return result;
    }

    public String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append(String.format("%02x", b));
        }
        return sb.toString();
    }

    // public static void main(String[] args) {

    //     //String algorithm = "SHA-256"; // if you perfer SHA-2
    //     String algorithm = "SHA-256";

    //     String pText = "criczlocal808";
    //     System.out.println(String.format(OUTPUT_FORMAT, "Input (string)", pText));
    //     System.out.println(String.format(OUTPUT_FORMAT, "Input (length)", pText.length()));

    //     ShaUtils shaUtils = new ShaUtils();
    //     byte[] shaInBytes = shaUtils.digest(pText.getBytes(UTF_8), algorithm);
    //     System.out.println(String.format(OUTPUT_FORMAT, algorithm + " (hex) ", shaUtils.bytesToHex(shaInBytes)));
    //     // fixed length, 32 bytes, 256 bits.
    //     System.out.println(String.format(OUTPUT_FORMAT, algorithm + " (length)", shaInBytes.length));
    // }
}