package com.phoenix.criczlocal.utils;

import java.util.Random;

import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.db.entity.User;
import com.phoenix.criczlocal.db.entity.UserOtp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * CommonUtils
 * 
 * @author Sharoon
 */
@Component
public class CommonUtils {

    @Value("${sms.enabled}")
    Boolean smsEnabled;

    @Value("${sms.default}")
    String smsDefault;

    @Autowired
    SMSUtils smsUtils;

    /**
     * Get total pages.
     * 
     * @param totalSize
     * @return totalPage
     */
    public Long getTotalPages(Long totalSize) {

        Long totalPages = 0L;
        if (totalSize == null) {
            return totalPages;
        }

        totalPages = totalSize != 0L ? (totalSize / CommonConstants.LIST_PER_PAGE) : totalPages;
        totalPages = (totalSize % CommonConstants.LIST_PER_PAGE) > 0 ? (totalPages + 1) : totalPages;

        return totalPages;
    }

    /**
     * Send the sms for given mobile number.
     * 
     * @return sms send four digit number.
     */
    public String sendSms(String mobileNumber) {

        if (smsEnabled) {
            return smsUtils.sendSMS(generateRandomNumber(), mobileNumber);
        } else {
            return smsDefault;
        }
    }

    /**
     * Send the sms for forgot password.
     * 
     * @return sms send four digit number.
     */
    public String sendSms(User user, UserOtp userOtp) {

        if (smsEnabled) {
            userOtp.setIsSmsEnabled(CommonConstants.ACTIVE);
            return smsUtils.sendSMS(generateRandomNumber(), user.getUserMobile());
        } else {
            userOtp.setIsSmsEnabled(CommonConstants.IN_ACTIVE);
            return smsDefault;
        }
    }

    /**
     * Get the 4 digit random number.
     * 
     * @return
     */
    public String generateRandomNumber() {

        Random random = new Random();
        return String.format("%04d", random.nextInt(10000));
    }
}