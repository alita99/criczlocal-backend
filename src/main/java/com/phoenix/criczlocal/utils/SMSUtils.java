package com.phoenix.criczlocal.utils;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * SMSUtils
 * 
 * @author Sharoon
 */
@Component
public class SMSUtils {
    // Find your Account Sid and Token at twilio.com/console
    // DANGER! This is insecure. See http://twil.io/secure
    // public static final String ACCOUNT_SID =
    // "ACfd6006c158f0318c9f0e19195082f039";
    // public static final String AUTH_TOKEN = "b366b48f4d52c3dcb2c1151fdf4aaa7b";

    @Value("${sms.enabled}")
    boolean smsEnabled;

    @Value("${sms.accountSid}")
    String ACCOUNT_SID;

    @Value("${sms.authToken}")
    String AUTH_TOKEN;

    @Value("${sms.fromNo}")
    String smsFromNo;

    /**
     * To send sms data.
     * 
     * @return
     */
    public String sendSMS(String randomDigit, String smsToNo) {

        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                // .creator(new PhoneNumber("+919047654156"), new PhoneNumber("+12063478482"),
                // randomDigit).create();
                .creator(new PhoneNumber("+91" + smsToNo), new PhoneNumber(smsFromNo), randomDigit).create();

        return randomDigit;
    }
}