package com.phoenix.criczlocal.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * CommonUtils
 * 
 * @author Sharoon
 */
@Component
public class EncryptionUtils {

    @Autowired
    ShaUtils shaUtils;

    public String securityEncrypt(String security) {
        return shaUtils.encrypt(security);
    }
}