package com.phoenix.criczlocal.exception;

/**
 * BaseException
 */
public class AuthenticationException extends Exception {

    AuthenticationException(String s) {
        super(s);  
    }
}