package com.phoenix.criczlocal.exception;

/**
 * BaseException
 */
public class ApplicationException extends Exception {

    ApplicationException(String s) {
        super(s);  
    }
}