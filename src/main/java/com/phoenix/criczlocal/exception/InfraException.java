package com.phoenix.criczlocal.exception;

/**
 * BaseException
 */
public class InfraException extends Exception {

    InfraException(String s) {
        super(s);  
    }
}