package com.phoenix.criczlocal.exception;

public class ValidationException extends Exception {

    ValidationException(String s) {
        super(s);  
    }
}