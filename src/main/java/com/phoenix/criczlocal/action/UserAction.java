package com.phoenix.criczlocal.action;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.phoenix.criczlocal.db.dao.PortalUserDAO;
import com.phoenix.criczlocal.db.dao.UserDAO;
import com.phoenix.criczlocal.db.entity.Portal;
import com.phoenix.criczlocal.db.entity.User;
import com.phoenix.criczlocal.model.PortalDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * UserAction
 * 
 * @Author: sharoon
 */
@Service("userAction")
public class UserAction extends BaseAction {

	@Autowired
	UserDAO userDAO;

	@Autowired
	PortalUserDAO portalUserDAO;

	@Transactional
	public List<User> getAllUsers() {
		return userDAO.getAll();
	}

	@Transactional
	public User getUser(Long id) {
		return userDAO.get(id);
	}

	@Transactional
	public User saveUser(User user) {
		return userDAO.save(user);
	}

	@Transactional
	public User updateUser(User user) {
		userDAO.update(user);
		return user;
	}

	@Transactional
	public void deleteUser(Long id) {
		userDAO.delete(id);
	}

	/**
	 * Get the users with same mobile number.
	 * @param mobileNo
	 * @return
	 */
	@Transactional
	public List<User> getListUserByMobileNo(String mobileNo) {
		return userDAO.getListUserByMobileNo(mobileNo);
	}

	/**
	 * Get user portals.
	 * @param user
	 * @return
	 */
	@Transactional
	public List<PortalDTO> getUserPortals(User user) {
		List<Portal> portals = portalUserDAO.getUserPortals(user);
		List<PortalDTO> portalDTOs = new ArrayList<PortalDTO>();

		if (portals != null) {
			for(Portal portal: portals) {
				portalDTOs.add(convert(portal, PortalDTO.class));
			}
		}
		return portalDTOs;
	}
}