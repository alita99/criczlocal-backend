package com.phoenix.criczlocal.action;

import com.phoenix.criczlocal.base.BeanConversionBase;
import com.phoenix.criczlocal.db.dao.CommonDAO;
import com.phoenix.criczlocal.utils.CommonUtils;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * BaseAction
 * 
 * @Author: sharoon
 */
public class BaseAction extends BeanConversionBase {

    @Autowired
    CommonDAO commonDAO;

    @Autowired
    CommonUtils commonUtils;

}