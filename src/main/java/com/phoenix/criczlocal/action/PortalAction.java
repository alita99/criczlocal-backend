package com.phoenix.criczlocal.action;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.db.dao.PortalDAO;
import com.phoenix.criczlocal.db.dao.PortalRuleDAO;
import com.phoenix.criczlocal.db.dao.PortalUserDAO;
import com.phoenix.criczlocal.db.entity.Portal;
import com.phoenix.criczlocal.db.entity.PortalRule;
import com.phoenix.criczlocal.db.entity.User;
import com.phoenix.criczlocal.db.exception.RecordNotFoundException;
import com.phoenix.criczlocal.model.PortalDTO;
import com.phoenix.criczlocal.model.PortalRuleDTO;
import com.phoenix.criczlocal.model.UserDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * PortalAction
 * 
 * @Author: sharoon
 */
@Service("portalAction")
public class PortalAction extends BaseAction {

    @Autowired
    PortalDAO portalDAO;

    @Autowired
    PortalUserDAO portalUserDAO;

    @Autowired
    PortalRuleDAO portalRuleDAO;

    /**
     * Get portal Users.
     * 
     * @param portalId
     * @return list of users.
     * @throws RecordNotFoundException
     */
    @Transactional
    public List<UserDTO> getPortalUsers(Long portalId) throws RecordNotFoundException {

        Portal portal = portalDAO.get(portalId);

        if (isNull(portal)) {
            throw new RecordNotFoundException("Need Data !!");
        }

        List<User> userDetails = portalUserDAO.getPortalUsers(portal);
        List<UserDTO> userDTOs = new ArrayList<UserDTO>();

        if (userDetails != null) {
            for (User user : userDetails) {
                userDTOs.add(convert(user, UserDTO.class));
            }
        }
        return userDTOs;
    }

    /**
     * Get portal Users.
     * 
     * @param portalId
     * @return list of users.
     * @throws RecordNotFoundException
     */
    @Transactional
    public User checkUserPortal(Long portalId, Long userId) throws RecordNotFoundException {

        Portal portal = portalDAO.get(portalId);

        if (isNull(portal)) {
            throw new RecordNotFoundException("Need Data !!");
        }

        User user = commonDAO.get(User.class, userId);
        User checkUser = portalUserDAO.checkUserPortal(portal, user);

        return checkUser;
    }

    /**
     * Get portal.
     * 
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public PortalDTO getPortal(Long portalId) throws RecordNotFoundException {

        Portal portal = portalDAO.get(portalId);

        if (isNull(portal)) {
            throw new RecordNotFoundException("Need Data !!");
        }

        return convert(portal, PortalDTO.class);
    }

    /**
     * Update the portal.
     * 
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public PortalDTO updatePortal(Long portalId, PortalDTO portalDTO) throws RecordNotFoundException {

        Portal portal = convert(portalDTO, Portal.class);

        if (isNull(portal)) {
            throw new RecordNotFoundException("Need Data !!");
        }

        portal.setCreatedBy(commonDAO.get(User.class, portalDTO.getCreatedBy()));
        portalDAO.saveOrUpdate(portal);
        return portalDTO;
    }

    /**
     * Delete the portal by portalId.
     * 
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public Boolean deletePortal(Long portalId) throws RecordNotFoundException {

        Portal portal = portalDAO.get(portalId);

        if (isNull(portal)) {
            throw new RecordNotFoundException("Need Data !!");
        }

        portal.setIsDelete(CommonConstants.DELETE);
        portalDAO.saveOrUpdate(portal);
        return Boolean.TRUE;
    }

    /**
     * Get portal rules.
     * 
     * @param portalId
     * @return
     */
    @Transactional
    public List<PortalRuleDTO> getPortalRules(Long portalId) {

        List<PortalRule> portalRules = portalRuleDAO.getPortalRules(portalDAO.get(portalId));
        return convertList(portalRules, PortalRuleDTO.class);
    }
}