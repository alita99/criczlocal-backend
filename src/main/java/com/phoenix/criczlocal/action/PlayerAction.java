package com.phoenix.criczlocal.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.constant.WebConstants;
import com.phoenix.criczlocal.db.dao.PlayerDAO;
import com.phoenix.criczlocal.db.entity.Gender;
import com.phoenix.criczlocal.db.entity.Player;
import com.phoenix.criczlocal.db.entity.PlayerType;
import com.phoenix.criczlocal.db.entity.Portal;
import com.phoenix.criczlocal.db.entity.User;
import com.phoenix.criczlocal.db.exception.RecordNotFoundException;
import com.phoenix.criczlocal.model.PlayerDTO;
import com.phoenix.criczlocal.model.PlayerTypeDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * PlayerAction
 * 
 * @Author: sharoon
 */
@Service("PlayerAction")
public class PlayerAction extends BaseAction {

    @Autowired
    PlayerDAO playerDAO;

    /**
     * Save the player details.
     * 
     * @param playerDTO
     * @param portalId
     * @return
     */
    @Transactional
    public PlayerDTO savePlayer(PlayerDTO playerDTO, Long portalId, Long userId) {
        Player player = convert(playerDTO, Player.class);
        player.setSignature(playerDTO.getName());
        player.setPlayerType(commonDAO.get(PlayerType.class, playerDTO.getPlayerTypeId()));
        player.setGender(commonDAO.get(Gender.class, playerDTO.getGenderId()));
        player.setPortal(commonDAO.get(Portal.class, portalId));
        player.setCreatedBy(commonDAO.get(User.class, userId));
        player.setIsActive(CommonConstants.ACTIVE);
        player.setIsDelete(CommonConstants.EXISIT);
        playerDAO.save(player);
        playerDTO = convert(player, PlayerDTO.class);
        return playerDTO;
    }

    /**
     * Save the player details.
     * 
     * @param playerDTO
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public PlayerDTO updatePlayer(PlayerDTO playerDTO, Long portalId) throws RecordNotFoundException {

        if (isNull(playerDTO)) {
            throw new RecordNotFoundException("Need Data !!");
        }

        Player player = convert(playerDTO, Player.class);

        if (isNull(player) || !player.getPortal().getPortalId().equals(portalId)) {
            throw new RecordNotFoundException("Invalid data !!");
        }

        player.setPlayerType(commonDAO.get(PlayerType.class, playerDTO.getPlayerTypeId()));
        player.setGender(commonDAO.get(Gender.class, playerDTO.getGenderId()));
        player.setPortal(commonDAO.get(Portal.class, portalId));
        player.setCreatedBy(commonDAO.get(User.class, playerDTO.getCreatedBy()));
        playerDAO.saveOrUpdate(player);
        return convert(player, PlayerDTO.class);
    }

    /**
     * Save the player details.
     * 
     * @param playerDTO
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public Boolean deletePlayer(Long playerId, Long portalId) throws RecordNotFoundException {
        Player player = playerDAO.get(playerId);

        if (isNull(player) || !player.getPortal().getPortalId().equals(portalId)) {
            throw new RecordNotFoundException("Invalid data !!");
        }

        // player.setIsDelete(CommonConstants.DELETE);
        playerDAO.delete(player);
        return Boolean.TRUE;
    }

    /**
     * Delete multiple players.
     * 
     * @param playerIds
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public Boolean deletePlayer(List<Long> playerIds, Long portalId) throws RecordNotFoundException {

        if (isEmpty(playerIds)) {
            throw new RecordNotFoundException("Need Data !!");
        }

        for (long playerId : playerIds) {

            Player player = playerDAO.get(playerId);

            if (isNull(player) || !player.getPortal().getPortalId().equals(portalId)) {
                throw new RecordNotFoundException("Invalid data !!");
            }

            playerDAO.delete(player);
        }

        return Boolean.TRUE;
    }

    /**
     * Get the player details.
     * 
     * @param playerDTO
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public PlayerDTO getPlayer(Long playerId, Long portalId) throws RecordNotFoundException {
        Player player = playerDAO.get(playerId);

        if (isNull(player) || !player.getPortal().getPortalId().equals(portalId)) {
            throw new RecordNotFoundException("Invalid data !!");
        }

        return convert(player, PlayerDTO.class);
    }

    /**
     * Get the portal players.
     * 
     * @param portalId
     * @return list of players.
     */
    @Transactional
    public Map<String, Object> getPlayers(Long portalId, Integer page) {
        Portal portal = commonDAO.get(Portal.class, portalId);
        List<Player> players = playerDAO.getPlayers(portal, page);

        Long toTalSize = playerDAO.getPlayerSize(portal);
        Long totalpages = commonUtils.getTotalPages(toTalSize);

        List<PlayerDTO> playerDTOs = convertList(players, PlayerDTO.class);

        Map<String, Object> returnData = new HashMap<String, Object>();
        returnData.put(WebConstants.PLAYER_DETAILS, playerDTOs);
        returnData.put(WebConstants.CURRENT_PAGE, page);
        returnData.put(WebConstants.TOTAL_PAGE, totalpages);

        return returnData;
    }

    /**
     * Get the player types.
     * 
     * @param portalId
     * @return
     */
    @Transactional
    public List<PlayerTypeDTO> getPlayerTypes(Long portalId) {
        List<PlayerType> playerTypes = commonDAO.getAll(PlayerType.class);

        List<PlayerTypeDTO> playerTypeDTOs = convertList(playerTypes, PlayerTypeDTO.class);
        return playerTypeDTOs;
    }
}
