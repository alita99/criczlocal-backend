package com.phoenix.criczlocal.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.constant.WebConstants;
import com.phoenix.criczlocal.db.dao.TeamDAO;
import com.phoenix.criczlocal.db.dao.TeamPlayerDAO;
import com.phoenix.criczlocal.db.entity.Player;
import com.phoenix.criczlocal.db.entity.PlayerType;
import com.phoenix.criczlocal.db.entity.Portal;
import com.phoenix.criczlocal.db.entity.Team;
import com.phoenix.criczlocal.db.entity.TeamPlayer;
import com.phoenix.criczlocal.db.entity.User;
import com.phoenix.criczlocal.db.exception.RecordNotFoundException;
import com.phoenix.criczlocal.model.TeamDTO;
import com.phoenix.criczlocal.model.TeamPlayerDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * TeamAction
 * 
 * @Author: sharoon
 */
@Service("TeamAction")
public class TeamAction extends BaseAction {

    @Autowired
    TeamDAO teamDAO;

    @Autowired
    TeamPlayerDAO teamPlayerDAO;

    /**
     * Save the team.
     * 
     * @param teamDTO
     * @param portalId
     * @param user
     * @return
     */
    @Transactional
    public TeamDTO save(TeamDTO teamDTO, Long portalId, User user) {

        Portal portal = commonDAO.get(Portal.class, portalId);
        Team team = convert(teamDTO, Team.class);
        team.setPortal(portal);
        team.setIsDelete(CommonConstants.EXISIT);
        team.setIsActive(CommonConstants.ACTIVE);
        team.setCreatedBy(user);
        team.setSignature(team.getTeamName());

        List<TeamPlayer> teamPlayers = new ArrayList<TeamPlayer>();

        if (!isNull(teamDTO.getTeamPlayers())) {
            for (TeamPlayerDTO teamPlayerDTO : teamDTO.getTeamPlayers()) {
                TeamPlayer teamPlayer = convert(teamPlayerDTO, TeamPlayer.class);
                teamPlayer.setPlayer(commonDAO.get(Player.class, teamPlayerDTO.getPlayerId()));
                teamPlayer.setPlayerType(commonDAO.get(PlayerType.class, teamPlayerDTO.getPlayerTypeId()));
                teamPlayer.setPortal(portal);
                teamPlayer.setTeam(team);
                teamPlayer.setIsDelete(CommonConstants.EXISIT);
                teamPlayer.setIsActive(CommonConstants.ACTIVE);
                teamPlayer.setCreatedBy(user);
                teamPlayers.add(teamPlayer);
            }
        }

        team.setTeamPlayers(teamPlayers);
        teamDAO.save(team);

        teamDTO = convert(team, TeamDTO.class);
        return teamDTO;
    }

    /**
     * Update the team.
     * 
     * @param teamDTO
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public TeamDTO updateTeam(TeamDTO teamDTO, Long portalId, User user) throws RecordNotFoundException {

        if (isNull(teamDTO)) {
            throw new RecordNotFoundException("Need Data !!");
        }

        Team team = convert(teamDTO, Team.class);

        if (isNull(team) || !team.getPortal().getPortalId().equals(portalId)) {
            throw new RecordNotFoundException("Invalid data !!");
        }

        Portal portal = commonDAO.get(Portal.class, portalId);

        team.setPortal(portal);
        team.setCreatedBy(user);

        List<TeamPlayer> teamPlayers = new ArrayList<TeamPlayer>();
        if (!isNull(teamDTO.getTeamPlayers())) {
            for (TeamPlayerDTO teamPlayerDTO : teamDTO.getTeamPlayers()) {

                if (!isNull(teamPlayerDTO.getIsDelete())
                        && teamPlayerDTO.getIsDelete().equals(CommonConstants.DELETE)) {

                    if (!isNull(teamPlayerDTO.getTeamPlayerId())) {
                        teamPlayerDAO.delete(commonDAO.get(TeamPlayer.class, teamPlayerDTO.getTeamPlayerId()));
                    }
                    continue;
                }

                TeamPlayer teamPlayer = convert(teamPlayerDTO, TeamPlayer.class);
                teamPlayer.setPlayer(commonDAO.get(Player.class, teamPlayerDTO.getPlayerId()));
                teamPlayer.setPlayerType(commonDAO.get(PlayerType.class, teamPlayerDTO.getPlayerTypeId()));
                teamPlayer.setPortal(portal);
                teamPlayer.setTeam(team);
                teamPlayer.setCreatedBy(user);

                // newly creating
                if (isNull(teamPlayer.getTeamPlayerId())) {
                    teamPlayer.setIsDelete(CommonConstants.EXISIT);
                    teamPlayer.setIsActive(CommonConstants.ACTIVE);
                }

                if (!isNull(teamPlayer.getTeamPlayerId())) {
                    teamPlayerDAO.update(teamPlayer);
                } else {
                    teamPlayers.add(teamPlayer);
                }
            }
        }
        team.setTeamPlayers(teamPlayers.size() == 0 ? null : teamPlayers);
        teamDAO.update(team);
        return convert(commonDAO.get(Team.class, team.getTeamId()), TeamDTO.class);
    }

    /**
     * Delete the team.
     * 
     * @param teamId
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public Boolean deleteTeam(Long teamId, Long portalId) throws RecordNotFoundException {

        Team team = commonDAO.get(Team.class, teamId);
        if (isNull(team) || !team.getPortal().getPortalId().equals(portalId)) {
            throw new RecordNotFoundException("Invalid data !!");
        }

        teamDAO.delete(team);
        return true;
    }

    /**
     * Delete the multiple teams.
     * 
     * @param teamId
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public Boolean deleteTeam(List<Long> teamIds, Long portalId) throws RecordNotFoundException {

        if (isEmpty(teamIds)) {
            throw new RecordNotFoundException("Need Data !!");
        }

        for (Long teamId : teamIds) {

            Team team = commonDAO.get(Team.class, teamId);

            if (isNull(team) || !team.getPortal().getPortalId().equals(portalId)) {
                throw new RecordNotFoundException("Invalid data !!");
            }
            teamDAO.delete(team);
        }

        return true;
    }

    /**
     * Get the team.
     * 
     * @param teamId
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public TeamDTO getTeam(Long teamId, Long portalId) throws RecordNotFoundException {

        Team team = teamDAO.get(teamId);

        if (isNull(team) || !team.getPortal().getPortalId().equals(portalId)) {
            throw new RecordNotFoundException("Invalid data !!");
        }

        return convert(team, TeamDTO.class);
    }

    /**
     * Get the team based of the respective portal.
     * 
     * @param portalId
     * @param page
     * @return
     */
    @Transactional
    public Map<String, Object> getTeams(Long portalId, Integer page) {

        try {
            Portal portal = commonDAO.get(Portal.class, portalId);
            List<Team> teams = teamDAO.getTeams(portal, page);

            Long toTalSize = teamDAO.getTeamSize(portal);
            Long totalpages = commonUtils.getTotalPages(toTalSize);
            List<TeamDTO> teamDTOs = convertList(teams, TeamDTO.class);
            Map<String, Object> returnData = new HashMap<String, Object>();

            returnData.put(WebConstants.TEAM_DETAILS, teamDTOs);
            returnData.put(WebConstants.CURRENT_PAGE, page);
            returnData.put(WebConstants.TOTAL_PAGE, totalpages);

            return returnData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}