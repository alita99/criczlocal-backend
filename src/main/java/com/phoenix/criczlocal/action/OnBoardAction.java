package com.phoenix.criczlocal.action;

import java.util.List;

import javax.transaction.Transactional;
import javax.xml.bind.ValidationException;

import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.constant.RoleConstants;
import com.phoenix.criczlocal.db.dao.CommonDAO;
import com.phoenix.criczlocal.db.dao.PortalDAO;
import com.phoenix.criczlocal.db.dao.PortalRuleDAO;
import com.phoenix.criczlocal.db.dao.PortalUserDAO;
import com.phoenix.criczlocal.db.dao.StandardRuleDAO;
import com.phoenix.criczlocal.db.dao.UserDAO;
import com.phoenix.criczlocal.db.dao.UserOtpDAO;
import com.phoenix.criczlocal.db.dao.UserSecurityDAO;
import com.phoenix.criczlocal.db.entity.Portal;
import com.phoenix.criczlocal.db.entity.PortalRule;
import com.phoenix.criczlocal.db.entity.PortalUser;
import com.phoenix.criczlocal.db.entity.Role;
import com.phoenix.criczlocal.db.entity.StandardRule;
import com.phoenix.criczlocal.db.entity.User;
import com.phoenix.criczlocal.db.entity.UserOtp;
import com.phoenix.criczlocal.db.entity.UserSecurity;
import com.phoenix.criczlocal.db.exception.RecordNotFoundException;
import com.phoenix.criczlocal.model.UserDTO;
import com.phoenix.criczlocal.model.auth.OnBoardUser;
import com.phoenix.criczlocal.security.jwt.JwtResponse;
import com.phoenix.criczlocal.security.jwt.JwtTokenUtil;
import com.phoenix.criczlocal.utils.EncryptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * OnBoardAction
 * 
 * @Author: sharoon
 */
@Service("OnBoardAction")
public class OnBoardAction extends BaseAction {

    @Autowired
    CommonDAO commonDAO;

    @Autowired
    UserDAO userDAO;

    @Autowired
    UserSecurityDAO userSecurityDAO;

    @Autowired
    PortalDAO portalDAO;

    @Autowired
    PortalUserDAO portalUserDAO;

    @Autowired
    UserOtpDAO userOtpDAO;

    @Autowired
    StandardRuleDAO standardRuleDAO;

    @Autowired
    PortalRuleDAO portalRuleDAO;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Autowired
    EncryptionUtils encryptionUtils;

    /**
     * To register the user.
     * 
     * @param onBoardUser
     * @return User the newly created user.
     * @throws ValidationException
     * @throws RecordNotFoundException
     */
    @Transactional
    public User registerUser(OnBoardUser onBoardUser) throws ValidationException, RecordNotFoundException {

        User user = new User();
        user.setUserName(onBoardUser.getUserName());
        user.setUserMobile(onBoardUser.getMobileNo());
        user.setUserEmail(onBoardUser.getEmail());
        user.setUserSignature(onBoardUser.getUserName());
        user.setIsActive(CommonConstants.IN_ACTIVE);
        user.setIsDelete(CommonConstants.EXISIT);
        userDAO.save(user);

        UserSecurity userSecurity = new UserSecurity();
        userSecurity.setSecurityKey(encryptionUtils.securityEncrypt(onBoardUser.getSecretKey()));
        userSecurity.setUser(user);
        userSecurity.setIsActive(CommonConstants.ACTIVE);
        userSecurityDAO.save(userSecurity);

        Portal portal = new Portal();
        portal.setPortalName(onBoardUser.getPortalName());
        portal.setPortalSignature(onBoardUser.getPortalName());
        portal.setCreatedBy(user);
        portal.setIsActive(CommonConstants.ACTIVE);
        portal.setIsDelete(CommonConstants.EXISIT);
        portalDAO.save(portal);

        PortalUser portalUser = new PortalUser();
        portalUser.setPortal(portal);
        portalUser.setUser(user);
        portalUser.setRole(commonDAO.get(Role.class, RoleConstants.ADMIN.getValue()));
        portalUser.setCreatedBy(user);
        portalUser.setIsActive(CommonConstants.ACTIVE);
        portalUserDAO.save(portalUser);

        setupEnvironment(user, portal);
        sendOtp(user.getUserMobile());
        return user;
    }

    /**
     * Verify the otp.
     * 
     * @param mobileNumber
     * @param otp
     * @return
     * @throws RecordNotFoundException
     * @throws ValidationException
     */
    @Transactional
    public User verifyUser(Long userId, String otp) throws RecordNotFoundException, ValidationException {
        User user = userDAO.get(userId);
        if (user == null) {
            throw new RecordNotFoundException("User not found");
        }

        // Retrive otp from db
        UserOtp userOtp = userOtpDAO.getActiveUserOtp(user);

        if (userOtp == null || otp == null || !userOtp.getOtp().equals(otp)) {
            throw new ValidationException("Otp invalid");
        }

        userOtp.setIsVerified(CommonConstants.IN_ACTIVE);
        userOtp.setIsActive(CommonConstants.IN_ACTIVE);
        userOtpDAO.saveOrUpdate(userOtp);

        user.setIsActive(CommonConstants.ACTIVE);
        userDAO.update(user);
        return user;
    }

    /**
     * Set up the default environment for user.
     * 
     * @param user
     * @param portal
     * @return
     */
    private Boolean setupEnvironment(User user, Portal portal) {

        // Standard rules to portal rules.
        List<StandardRule> standardRules = standardRuleDAO.getActiveStandardRule();
        for (StandardRule standardRule : standardRules) {
            PortalRule portalRule = convert(standardRule, PortalRule.class);
            portalRule.setPortal(portal);
            portalRule.setCreatedBy(user);
            portalRuleDAO.save(portalRule);
        }

        return true;
    }

    /**
     * Prepare the json token for the user.
     * 
     * @param user
     * @return JwtResponse
     */
    public JwtResponse prepareJwtResponse(User user) {
        try {

            JwtResponse jwtResponse = new JwtResponse();
            if (user != null && user.getIsActive().equals(CommonConstants.ACTIVE)) {
                String token = jwtTokenUtil.generateToken(user);
                jwtResponse = new JwtResponse(token);
            }
            jwtResponse.setUserDetails(convert(user, UserDTO.class));
            return jwtResponse;
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Resend the otp to the respective user.
     * 
     * @param mobileNumber
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public Boolean reSendUserOtp(Long userId) throws RecordNotFoundException {

        User user = userDAO.get(userId);
        if (user == null) {
            throw new RecordNotFoundException("User not found");
        }

        return sendOtp(user.getUserMobile());
    }

    /**
     * Resend the otp to mobile number.
     * 
     * @param mobileNumber
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public Boolean resendOtp(String mobileNumber) throws RecordNotFoundException {
        return sendOtp(mobileNumber);
    }

    /**
     * Send otp for the respecctive mobile number.
     * 
     * @param mobileNumber
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public Boolean sendOtp(String mobileNumber) throws RecordNotFoundException {
        User user = userDAO.getUserByMobileNumber(mobileNumber);
        if (user == null) {
            throw new RecordNotFoundException("User not found");
        }

        // Retrive otp from db
        UserOtp userOtp = userOtpDAO.getUserOtp(user);

        if (userOtp == null) {
            userOtp = new UserOtp();
            userOtp.setUser(user);
        }

        String otp = commonUtils.sendSms(user, userOtp);
        userOtp.setOtp(otp);
        userOtp.setIsActive(CommonConstants.ACTIVE);
        userOtp.setIsVerified(CommonConstants.IN_ACTIVE);
        userOtpDAO.saveOrUpdate(userOtp);
        return true;
    }

    /**
     * Verify the otp.
     * 
     * @param mobileNumber
     * @param otp
     * @return
     * @throws RecordNotFoundException
     * @throws ValidationException
     */
    @Transactional
    public User verifyOtp(String mobileNumber, String otp) throws RecordNotFoundException, ValidationException {
        User user = userDAO.getUserByMobileNumber(mobileNumber);
        if (user == null) {
            throw new RecordNotFoundException("User not found");
        }

        // Retrive otp from db
        UserOtp userOtp = userOtpDAO.getActiveUserOtp(user);

        if (userOtp == null || otp == null || !userOtp.getOtp().equals(otp)) {
            throw new ValidationException("Otp invalid");
        }

        userOtp.setIsVerified(CommonConstants.ACTIVE);
        userOtpDAO.saveOrUpdate(userOtp);
        return user;
    }

    /**
     * Reset the security.
     * 
     * @param mobileNumber
     * @param securityKey
     * @return
     * @throws RecordNotFoundException
     * @throws ValidationException
     * @throws CloneNotSupportedException
     */
    @Transactional
    public Boolean resetSecurity(String mobileNumber, String secretKey)
            throws RecordNotFoundException, ValidationException, CloneNotSupportedException {

        User user = userDAO.getUserByMobileNumber(mobileNumber);
        if (user == null) {
            throw new RecordNotFoundException("User not found");
        }

        // Retrive otp from db
        UserOtp userOtp = userOtpDAO.getActiveUserOtp(user);

        if (userOtp == null || !userOtp.getIsVerified().equals(CommonConstants.ACTIVE)) {
            throw new ValidationException("Otp not verified yet");
        }

        UserSecurity userSecurityOld = userSecurityDAO.getActiveUserSecurity(user);
        UserSecurity userSecurityNew = userSecurityOld.clone();

        userSecurityOld.setIsActive(CommonConstants.IN_ACTIVE);
        userSecurityDAO.saveOrUpdate(userSecurityOld);

        userSecurityNew.setSecurityId(null);
        userSecurityNew.setSecurityKey(encryptionUtils.securityEncrypt(secretKey));
        userSecurityNew.setCreatedOn(null);
        userSecurityNew.setUpdatedOn(null);
        userSecurityDAO.saveOrUpdate(userSecurityNew);

        userOtp.setIsVerified(CommonConstants.IN_ACTIVE);
        userOtp.setIsActive(CommonConstants.IN_ACTIVE);
        userOtpDAO.saveOrUpdate(userOtp);

        return true;
    }
}