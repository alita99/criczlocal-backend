package com.phoenix.criczlocal.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import javax.xml.bind.ValidationException;

import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.constant.WebConstants;
import com.phoenix.criczlocal.db.dao.MatchDAO;
import com.phoenix.criczlocal.db.entity.Match;
import com.phoenix.criczlocal.db.entity.Portal;
import com.phoenix.criczlocal.db.entity.PortalRule;
import com.phoenix.criczlocal.db.entity.Team;
import com.phoenix.criczlocal.db.entity.User;
import com.phoenix.criczlocal.db.exception.RecordNotFoundException;
import com.phoenix.criczlocal.model.MatchDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * MatchAction
 * 
 * @Author: sharoon
 */
@Service("MatchAction")
public class MatchAction extends BaseAction {

    @Autowired
    MatchDAO matchDAO;

    /**
     * Save the match details.
     * 
     * @param matchDTO
     * @param portalId
     * @param userId
     * @return
     * @throws ValidationException
     * @throws RecordNotFoundException
     */
    @Transactional
    public MatchDTO saveMatch(MatchDTO matchDTO, Long portalId, Long userId)
            throws ValidationException, RecordNotFoundException {

        validateMatch(matchDTO);
        Match match = convert(matchDTO, Match.class);

        if (isNull(match)) {
            throw new RecordNotFoundException("Need Data !!");
        }

        match.setTeam1(commonDAO.get(Team.class, matchDTO.getTeamId1()));
        match.setTeam2(commonDAO.get(Team.class, matchDTO.getTeamId2()));

        if (matchDTO.getRuleId() != null)
            match.setRule(commonDAO.get(PortalRule.class, matchDTO.getRuleId()));

        match.setPortal(commonDAO.get(Portal.class, portalId));
        match.setCreatedBy(commonDAO.get(User.class, userId));
        match.setIsActive(CommonConstants.ACTIVE);
        match.setIsDelete(CommonConstants.EXISIT);
        matchDAO.saveOrUpdate(match);
        return convert(match, MatchDTO.class);
    }

    /**
     * Update the match.
     * 
     * @param matchDTO
     * @param portalId
     * @param userId
     * @return
     * @throws ValidationException
     * @throws RecordNotFoundException
     */
    @Transactional
    public MatchDTO updateMatch(MatchDTO matchDTO, Long matchId, Long portalId, Long userId)
            throws ValidationException, RecordNotFoundException {

        validateMatch(matchDTO);
        Match match = convert(matchDTO, Match.class);

        if (isNull(match)) {
            throw new RecordNotFoundException("Need Data !!");
        }

        if (!match.getMatchId().equals(matchId) || !match.getPortal().getPortalId().equals(portalId)) {
            throw new RecordNotFoundException("Invalid data !!");
        }

        match.setTeam1(commonDAO.get(Team.class, matchDTO.getTeamId1()));
        match.setTeam2(commonDAO.get(Team.class, matchDTO.getTeamId2()));

        if (matchDTO.getRuleId() != null) {
            match.setRule(commonDAO.get(PortalRule.class, matchDTO.getRuleId()));
        }

        if (matchDTO.getCreatedBy() != null) {
            match.setCreatedBy(commonDAO.get(User.class, matchDTO.getCreatedBy()));
        }

        match.setPortal(commonDAO.get(Portal.class, portalId));
        matchDAO.saveOrUpdate(match);
        return convert(match, MatchDTO.class);
    }

    /**
     * Validate the match.
     * 
     * @param matchDTO
     * @throws ValidationException
     */
    public void validateMatch(MatchDTO matchDTO) throws ValidationException {
        if (matchDTO.getTeamId1() == null || matchDTO.getTeamId2() == null) {
            throw new ValidationException("Teams are mandatory");
        }

        if (matchDTO.getTeamId1().equals(matchDTO.getTeamId2())) {
            throw new ValidationException("Teams need to be different");
        }
    }

    /**
     * Get the match.
     * 
     * @param matchId
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public MatchDTO getMatch(Long matchId, Long portalId) throws RecordNotFoundException {

        Match match = matchDAO.get(matchId);

        if (isNull(match) || !match.getPortal().getPortalId().equals(portalId)) {
            throw new RecordNotFoundException("Invalid data !!");
        }

        return convert(match, MatchDTO.class);
    }

    /**
     * Delete the respective match.
     * 
     * @param matchId
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public boolean deleteMatch(Long matchId, Long portalId) throws RecordNotFoundException {

        Match match = matchDAO.get(matchId);

        if (isNull(match) || !match.getPortal().getPortalId().equals(portalId)) {
            throw new RecordNotFoundException("Invalid data !!");
        }
        matchDAO.delete(match);
        return true;
    }

    /**
     * Delete the matches.
     * 
     * @param matchIds
     * @param portalId
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public boolean deleteMatch(List<Long> matchIds, Long portalId) throws RecordNotFoundException {

        if (isEmpty(matchIds)) {
            throw new RecordNotFoundException("Need Data !!");
        }

        for (Long matchId : matchIds) {
            Match match = matchDAO.get(matchId);

            if (isNull(match) || !match.getPortal().getPortalId().equals(portalId)) {
                throw new RecordNotFoundException("Invalid data !!");
            }
            matchDAO.delete(match);
        }
        return true;
    }

    /**
     * Get the portal matches.
     * 
     * @param portalId
     * @param page
     * @return
     * @throws RecordNotFoundException
     */
    @Transactional
    public Map<String, Object> getPortalMatches(Long portalId, Integer page) throws RecordNotFoundException {

        Portal portal = commonDAO.get(Portal.class, portalId);

        Long toTalSize = matchDAO.getMatchSize(portal);
        Long totalpages = commonUtils.getTotalPages(toTalSize);

        if (isNull(portal)) {
            throw new RecordNotFoundException("Invalid data !!");
        }
        List<Match> matches = matchDAO.getPortalMatches(portal, page);

        List<MatchDTO> matcheDTOs = convertList(matches, MatchDTO.class);

        Map<String, Object> returnData = new HashMap<String, Object>();
        returnData.put(WebConstants.MATCH_DETAILS, matcheDTOs);
        returnData.put(WebConstants.CURRENT_PAGE, page);
        returnData.put(WebConstants.TOTAL_PAGE, totalpages);

        return returnData;
    }
}