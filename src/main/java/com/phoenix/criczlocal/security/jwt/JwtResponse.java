package com.phoenix.criczlocal.security.jwt;

import java.io.Serializable;

import com.phoenix.criczlocal.model.UserDTO;

public class JwtResponse implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;
	private String jwttoken;
	private UserDTO userDetails;

	public JwtResponse() {
	}

	public JwtResponse(String jwttoken) {
		this.jwttoken = jwttoken;
	}

	public String getToken() {
		return this.jwttoken;
	}

	public UserDTO getUserDetails() {
		return this.userDetails;
	}

	public void setUserDetails(UserDTO userDetails) {
		this.userDetails = userDetails;
	}

}