package com.phoenix.criczlocal.security.jwt;

import com.google.gson.Gson;
import com.phoenix.criczlocal.constant.ResponseConstant;
import com.phoenix.criczlocal.model.web.ErrorResponseBody;
import com.phoenix.criczlocal.model.web.ResponseBody;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * RestAccessDeniedHandler
 * 
 * @author Sharoon
 */
@Component
public class RestAccessDeniedHandler implements AccessDeniedHandler {

    @Autowired
    Gson gson;

    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
            AccessDeniedException e) throws IOException, ServletException {

        ResponseBody reponseBody = new ResponseBody();
        reponseBody.setStatusCode(HttpServletResponse.SC_UNAUTHORIZED);
        reponseBody.setIsSuccess(ResponseConstant.FAIL);

        ErrorResponseBody error = new ErrorResponseBody();
        error.setTitle("Unauthorized.");
        error.setMessage("Access Denied");
        reponseBody.setError(error);
        httpServletResponse.getOutputStream().println(gson.toJson(reponseBody));

    }
}