package com.phoenix.criczlocal.security.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.phoenix.criczlocal.api.BaseController;
import com.phoenix.criczlocal.utils.EncryptionUtils;

@RestController
@CrossOrigin
public class JwtAuthenticationController extends BaseController {

	@Autowired
	private JwtUserDetailsService userDetailsService;

	@Autowired
    EncryptionUtils encryptionUtils;

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

		try {
			JwtResponse jwtResponse = userDetailsService.authenticateUser(authenticationRequest.getUsername(),
			encryptionUtils.securityEncrypt(authenticationRequest.getPassword()));
			return returnResponse(jwtResponse);
		} catch (BadCredentialsException e) {
			return returnValidation("User name or password wrong", "User name or password wrong");
		} catch (Exception e) {
			return returnException(e);
		}
	}
}