package com.phoenix.criczlocal.security.jwt;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.phoenix.criczlocal.constant.ResponseConstant;
import com.phoenix.criczlocal.model.web.ErrorResponseBody;
import com.phoenix.criczlocal.model.web.ResponseBody;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

	private static final long serialVersionUID = -7858869558953243875L;

	@Autowired
	Gson gson;
	
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

		if (authException instanceof InsufficientAuthenticationException) {
			ResponseBody reponseBody = new ResponseBody();
            reponseBody.setStatusCode(HttpServletResponse.SC_UNAUTHORIZED);
            reponseBody.setIsSuccess(ResponseConstant.FAIL);

            ErrorResponseBody error = new ErrorResponseBody();
			error.setTitle("Unauthorized.");
            error.setMessage("Token invalid.");
			reponseBody.setError(error);
			response.getOutputStream().println(gson.toJson(reponseBody));

		}else if (authException instanceof AuthenticationCredentialsNotFoundException) {

			ResponseBody reponseBody = new ResponseBody();
            reponseBody.setStatusCode(HttpServletResponse.SC_UNAUTHORIZED);
            reponseBody.setIsSuccess(ResponseConstant.FAIL);

            ErrorResponseBody error = new ErrorResponseBody();
            error.setTitle("Unauthorized.");
            error.setMessage("Missing Authorization Header.");
			reponseBody.setError(error);
			response.getOutputStream().println(gson.toJson(reponseBody));

		} else {

			ResponseBody reponseBody = new ResponseBody();
            reponseBody.setStatusCode(HttpServletResponse.SC_UNAUTHORIZED);
            reponseBody.setIsSuccess(ResponseConstant.FAIL);

            ErrorResponseBody error = new ErrorResponseBody();
			error.setTitle("Unauthorized.");
            error.setMessage("Token invalid.");
			reponseBody.setError(error);
			response.getOutputStream().println(gson.toJson(reponseBody));
		}
	}
}