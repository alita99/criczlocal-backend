package com.phoenix.criczlocal.security.jwt;

import java.util.ArrayList;

import javax.transaction.Transactional;

import com.phoenix.criczlocal.action.BaseAction;
import com.phoenix.criczlocal.action.OnBoardAction;
import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.db.dao.UserDAO;
import com.phoenix.criczlocal.db.dao.UserSecurityDAO;
import com.phoenix.criczlocal.db.entity.UserSecurity;
import com.phoenix.criczlocal.db.exception.RecordNotFoundException;
import com.phoenix.criczlocal.model.UserDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService extends BaseAction implements UserDetailsService {

	@Autowired
	UserDAO userDAO;

	@Autowired
	UserSecurityDAO userSecurityDAO;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	OnBoardAction onBoardAction;

	@Transactional
	@Override
	public UserDetails loadUserByUsername(String userMobileNo) throws UsernameNotFoundException {

		com.phoenix.criczlocal.db.entity.User user = userDAO.getUserByMobileNumber(userMobileNo);
		if (user.getUserMobile().equals(userMobileNo)) {
			UserSecurity userSecurity = userSecurityDAO.getActiveUserSecurity(user);
			return new User(user.getUserMobile(), new BCryptPasswordEncoder().encode(userSecurity.getSecurityKey()),
					new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with mobile number: " + userMobileNo);
		}
	}

	/**
	 * load user by user id.
	 * 
	 * @param userId
	 * @return
	 * @throws UsernameNotFoundException
	 */
	@Transactional
	public UserDetails loadUserByUserId(Long userId) throws UsernameNotFoundException {

		com.phoenix.criczlocal.db.entity.User user = userDAO.get(userId);
		if (user.getUserId().equals(userId)) {
			UserSecurity userSecurity = userSecurityDAO.getActiveUserSecurity(user);
			return new User(user.getUserId().toString(),
					new BCryptPasswordEncoder().encode(userSecurity.getSecurityKey()), new ArrayList<>());
		} else {
			throw new UsernameNotFoundException("User not found with mobile number: " + userId);
		}
	}

	/**
	 * Authenticate user.
	 * 
	 * @param userName
	 * @param password
	 * @return
	 * @throws RecordNotFoundException
	 * @throws Exception
	 */
	@Transactional
	public JwtResponse authenticateUser(String userName, String password) throws RecordNotFoundException {
		authenticate(userName, password);
		com.phoenix.criczlocal.db.entity.User user = userDAO.getUserByMobileNumber(userName);

		JwtResponse jwtResponse = new JwtResponse();
		if (user != null && user.getIsActive().equals(CommonConstants.ACTIVE)) {
			String token = jwtTokenUtil.generateToken(user);
			jwtResponse = new JwtResponse(token);
		} else {
			onBoardAction.sendOtp(user.getUserMobile());
		}

		UserDTO userDTO = convert(user, UserDTO.class);
		jwtResponse.setUserDetails(userDTO);

		return jwtResponse;
	}

	/**
	 * Autenticate.
	 * 
	 * @param userName
	 * @param password
	 * @throws Exception
	 */
	private void authenticate(String userName, String password) {
		authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userName, password));
	}
}