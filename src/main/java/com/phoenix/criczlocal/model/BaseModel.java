package com.phoenix.criczlocal.model;

import java.io.Serializable;
import java.util.Date;

import com.phoenix.criczlocal.base.BeanConversionBase;

/**
 * BaseModel
 * 
 * @Author: sharoon
 */
public class BaseModel extends BeanConversionBase implements Serializable {

    protected Long version;
    protected Date createdOn;
    protected Date updatedOn;

    public Long getVersion() {
        return this.version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return this.updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
}