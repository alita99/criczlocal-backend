package com.phoenix.criczlocal.model;

/**
 * PlayerTypeDTO
 * 
 * @Author: sharoon
 */
public class PlayerTypeDTO extends BaseModel {

    Long playerTypeId;
    String name;
    String description;

    public Long getPlayerTypeId() {
        return this.playerTypeId;
    }

    public void setPlayerTypeId(Long playerTypeId) {
        this.playerTypeId = playerTypeId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}