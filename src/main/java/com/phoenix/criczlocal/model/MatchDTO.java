package com.phoenix.criczlocal.model;

import java.util.Date;

/**
 * MatchDTO
 * 
 * @Author: sharoon
 */
public class MatchDTO extends BaseModel {

    Long matchId;
    String matchName;
    Date startOn;
    Long teamId1;
    String teamName1;
    Long teamId2;
    String teamName2;
    Long ruleId;
    String ruleName;
    Long portalId;
    Long createdBy;
    Integer status;
    Integer isActive;
    Integer isDelete;

    public Long getMatchId() {
        return this.matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public String getMatchName() {
        return this.matchName;
    }

    public void setMatchName(String matchName) {
        this.matchName = matchName;
    }

    public Date getStartOn() {
        return this.startOn;
    }

    public void setStartOn(Date startOn) {
        this.startOn = startOn;
    }

    public Long getTeamId1() {
        return this.teamId1;
    }

    public void setTeamId1(Long teamId1) {
        this.teamId1 = teamId1;
    }

    public String getTeamName1() {
        return this.teamName1;
    }

    public void setTeamName1(String teamName1) {
        this.teamName1 = teamName1;
    }

    public Long getTeamId2() {
        return this.teamId2;
    }

    public void setTeamId2(Long teamId2) {
        this.teamId2 = teamId2;
    }

    public String getTeamName2() {
        return this.teamName2;
    }

    public void setTeamName2(String teamName2) {
        this.teamName2 = teamName2;
    }

    public Long getRuleId() {
        return this.ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getRuleName() {
        return this.ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public Long getPortalId() {
        return this.portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}