package com.phoenix.criczlocal.model;

import java.util.List;

/**
 * TeamDTO
 * 
 * @Author: sharoon
 */
public class TeamDTO extends BaseModel {

    Long teamId;
    String signature;
    String teamName;
    String teamShortName;
    Long portalId;
    Long createdBy;
    Integer isActive;
    Integer isDelete;
    List<TeamPlayerDTO> teamPlayers;

    public Long getTeamId() {
        return this.teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public String getSignature() {
        return this.signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getTeamName() {
        return this.teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamShortName() {
        return this.teamShortName;
    }

    public void setTeamShortName(String teamShortName) {
        this.teamShortName = teamShortName;
    }

    public Long getPortalId() {
        return this.portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public List<TeamPlayerDTO> getTeamPlayers() {
        return this.teamPlayers;
    }

    public void setTeamPlayers(List<TeamPlayerDTO> teamPlayers) {
        this.teamPlayers = teamPlayers;
    }
}