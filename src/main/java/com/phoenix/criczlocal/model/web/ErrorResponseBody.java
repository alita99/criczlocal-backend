package com.phoenix.criczlocal.model.web;

/**
 * ErrorResponseBody
 * @Author: sharoon
 */
public class ErrorResponseBody {
    
    private String title;
    private String message;
    private Object errorData;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getErrorData() {
        return this.errorData;
    }

    public void setErrorData(Object errorData) {
        this.errorData = errorData;
    }
}