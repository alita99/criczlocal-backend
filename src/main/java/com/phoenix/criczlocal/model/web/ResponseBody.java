package com.phoenix.criczlocal.model.web;

/**
 * ResponseBody
 * @Author: sharoon
 */
public class ResponseBody {
  
    private Boolean isSuccess;
    private Integer statusCode;
    private Object data;
    private ErrorResponseBody error;

    public Boolean isIsSuccess() {
        return this.isSuccess;
    }

    public Boolean getIsSuccess() {
        return this.isSuccess;
    }

    public Integer getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public void setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public ErrorResponseBody getError() {
        return this.error;
    }

    public void setError(ErrorResponseBody error) {
        this.error = error;
    }
}