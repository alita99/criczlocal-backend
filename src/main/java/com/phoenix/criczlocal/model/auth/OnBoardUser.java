package com.phoenix.criczlocal.model.auth;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.phoenix.criczlocal.model.BaseModel;

/**
 * OnBoardUser
 * @Author: sharoon
 */
public class OnBoardUser {

    @Size(min = 5, max = 30, message = "min = 5, max = 30")
    String userName;

    @Pattern(regexp = "^[0-9]{9,10}", message = "Only numeric")
    String mobileNo;

    @Size(min = 5, max = 30, message = "min = 5, max = 30")
    String portalName;

    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=\\S+$).{7,20}$", message = "Atleast one alphabet, atleast one numeric")
    String secretKey;

    String email;
    
    String countryCode;

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobileNo() {
        return this.mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPortalName() {
        return this.portalName;
    }

    public void setPortalName(String portalName) {
        this.portalName = portalName;
    }

    public String getSecretKey() {
        return this.secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getCountryCode() {
        return this.countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}