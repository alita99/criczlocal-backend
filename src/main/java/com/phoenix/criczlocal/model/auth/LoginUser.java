package com.phoenix.criczlocal.model.auth;

import com.phoenix.criczlocal.model.BaseModel;

/**
 * LoginUser
 */
public class LoginUser extends BaseModel{

    String mobileNo;
    String userName;
    String secrectKey;

    public String getMobileNo() {
        return this.mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSecrectKey() {
        return this.secrectKey;
    }

    public void setSecrectKey(String secrectKey) {
        this.secrectKey = secrectKey;
    }


}