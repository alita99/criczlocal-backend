package com.phoenix.criczlocal.model;

/**
 * PortalRuleDTO
 * 
 * @Author: sharoon
 */
public class PortalRuleDTO extends BaseModel {

    Long ruleId;
    String rulesName;
    Integer overs;
    Integer powerPlay;
    Integer players;
    Integer substitute;
    Long portalId;
    Long createdBy;
    Integer isActive;

    public Long getRuleId() {
        return this.ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }

    public String getRulesName() {
        return this.rulesName;
    }

    public void setRulesName(String rulesName) {
        this.rulesName = rulesName;
    }

    public Integer getOvers() {
        return this.overs;
    }

    public void setOvers(Integer overs) {
        this.overs = overs;
    }

    public Integer getPowerPlay() {
        return this.powerPlay;
    }

    public void setPowerPlay(Integer powerPlay) {
        this.powerPlay = powerPlay;
    }

    public Integer getPlayers() {
        return this.players;
    }

    public void setPlayers(Integer players) {
        this.players = players;
    }

    public Integer getSubstitute() {
        return this.substitute;
    }

    public void setSubstitute(Integer substitute) {
        this.substitute = substitute;
    }

    public Long getPortalId() {
        return this.portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }
}
