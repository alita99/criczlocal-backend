package com.phoenix.criczlocal.model;

import java.util.Date;

/**
 * PlayerDTO
 * 
 * @Author: sharoon
 */
public class PlayerDTO extends BaseModel {

    Long playerId;
    String signature;
    UserDTO user;
    String name;
    String email;
    String mobile;
    Long genderId;
    Date dob;
    Long playerTypeId;
    String playerTypeName;
    Long portalId;
    Long createdBy;
    Integer isActive;
    Integer isDelete;

    public Long getPlayerId() {
        return this.playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public String getSignature() {
        return this.signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public UserDTO getUser() {
        return this.user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Long getGenderId() {
        return this.genderId;
    }

    public void setGenderId(Long genderId) {
        this.genderId = genderId;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Long getPlayerTypeId() {
        return this.playerTypeId;
    }

    public void setPlayerTypeId(Long playerTypeId) {
        this.playerTypeId = playerTypeId;
    }

    public String getPlayerTypeName() {
        return this.playerTypeName;
    }

    public void setPlayerTypeName(String playerTypeName) {
        this.playerTypeName = playerTypeName;
    }

    public Long getPortalId() {
        return this.portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}