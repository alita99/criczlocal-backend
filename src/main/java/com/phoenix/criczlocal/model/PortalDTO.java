package com.phoenix.criczlocal.model;

public class PortalDTO extends BaseModel {

    Long portalId;
    String portalSignature;
    String portalName;
    Long createdBy;
    Integer isActive;
    Integer isDelete;

    public Long getPortalId() {
        return this.portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public String getPortalSignature() {
        return this.portalSignature;
    }

    public void setPortalSignature(String portalSignature) {
        this.portalSignature = portalSignature;
    }

    public String getPortalName() {
        return this.portalName;
    }

    public void setPortalName(String portalName) {
        this.portalName = portalName;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}