package com.phoenix.criczlocal.model;

/**
 * TeamPlayerDTO
 * 
 * @Author: sharoon
 */
public class TeamPlayerDTO extends BaseModel {

    private static final long serialVersionUID = 1L;

    Long teamPlayerId;
    Long teamId;
    Long playerId;
    String name;
    Long playerTypeId;
    String playerTypeName;
    Integer isCaptian;
    Integer isViceCaptian;
    Long createdBy;
    Long portalId;
    Integer isActive;
    Integer isDelete;

    public Long getTeamPlayerId() {
        return this.teamPlayerId;
    }

    public void setTeamPlayerId(Long teamPlayerId) {
        this.teamPlayerId = teamPlayerId;
    }

    public Long getTeamId() {
        return this.teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public Long getPlayerId() {
        return this.playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
   
    public Long getPlayerTypeId() {
        return this.playerTypeId;
    }

    public void setPlayerTypeId(Long playerTypeId) {
        this.playerTypeId = playerTypeId;
    }

    public String getPlayerTypeName() {
        return this.playerTypeName;
    }

    public void setPlayerTypeName(String playerTypeName) {
        this.playerTypeName = playerTypeName;
    }

    public Integer getIsCaptian() {
        return this.isCaptian;
    }

    public void setIsCaptian(Integer isCaptian) {
        this.isCaptian = isCaptian;
    }

    public Integer getIsViceCaptian() {
        return this.isViceCaptian;
    }

    public void setIsViceCaptian(Integer isViceCaptian) {
        this.isViceCaptian = isViceCaptian;
    }

    public Long getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getPortalId() {
        return this.portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }    
}