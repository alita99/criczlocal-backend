package com.phoenix.criczlocal.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * StandardRules
 * @Author: sharoon
 */
@Entity
@Table(name = "STANDARD_RULE")
public class StandardRule extends BaseEntity {
    
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "RULE_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long ruleId;

    @Column(name = "RULE_NAME")
    String rulesName;

    @Column(name = "OVERS")
    Integer overs;

    @Column(name = "POWER_PLAY")
    Integer powerPlay;

    @Column(name = "PLAYERS")
    Integer players;

    @Column(name = "SUBSTITUTE")
    Integer substitute;

    @Column(name = "IS_ACTIVE")
    Integer isActive;

    public Long getRuleId() {
        return this.ruleId;
    }

    public void setRuleId(Long ruleId) {
        this.ruleId = ruleId;
    }
    
    public String getRulesName() {
        return this.rulesName;
    }

    public void setRulesName(String rulesName) {
        this.rulesName = rulesName;
    }

    public Integer getOvers() {
        return this.overs;
    }

    public void setOvers(Integer overs) {
        this.overs = overs;
    }

    public Integer getPowerPlay() {
        return this.powerPlay;
    }

    public void setPowerPlay(Integer powerPlay) {
        this.powerPlay = powerPlay;
    }

    public Integer getPlayers() {
        return this.players;
    }

    public void setPlayers(Integer players) {
        this.players = players;
    }

    public Integer getSubstitute() {
        return this.substitute;
    }

    public void setSubstitute(Integer substitute) {
        this.substitute = substitute;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }
}