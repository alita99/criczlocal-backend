package com.phoenix.criczlocal.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * PortalUser
 * 
 * @Author: sharoon
 */
@Entity
@Table(name = "PORTAL_USER")
public class PortalUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PORTAL_USER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long portalUserId;

    @ManyToOne
    @JoinColumn(name = "PORTAL_ID")
    Portal portal;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    User user;

    @ManyToOne
    @JoinColumn(name = "ROLE_ID")
    Role role;

    @ManyToOne()
    @JoinColumn(name = "CREATED_BY")
    User createdBy;

    @Column(name = "IS_ACTIVE")
    Integer isActive;

    public Long getPortalUserId() {
        return this.portalUserId;
    }

    public void setPortalUserId(Long portalUserId) {
        this.portalUserId = portalUserId;
    }

    public Portal getPortal() {
        return this.portal;
    }

    public void setPortal(Portal portal) {
        this.portal = portal;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Role getRole() {
        return this.role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public User getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }
}