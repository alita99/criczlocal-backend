package com.phoenix.criczlocal.db.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import com.phoenix.criczlocal.base.BeanConversionBase;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * BaseEntity
 * 
 * @Author: sharoon
 */
@MappedSuperclass
public class BaseEntity extends BeanConversionBase implements Serializable {

    private static final long serialVersionUID = 1L;

    @Version
    @Column(name = "VERSION")
    protected Long version;

    @CreationTimestamp
    @Column(name = "CREATED_ON", updatable = false)
    protected Date createdOn;

    @UpdateTimestamp
    @Column(name = "UPDATED_ON")
    protected Date updatedOn;

    public Long getVersion() {
        return this.version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getCreatedOn() {
        return this.createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return this.updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
}