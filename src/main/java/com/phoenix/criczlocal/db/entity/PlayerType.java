package com.phoenix.criczlocal.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Portal
 * @Author: sharoon
 */
@Entity
@Table(name = "PLAYER_TYPE")
public class PlayerType extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PLAYER_TYPE_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long playerTypeId;

    @Column(name = "NAME")
    String name;

    @Column(name = "DESCRIPTION")
    String description;

    public Long getPlayerTypeId() {
        return this.playerTypeId;
    }

    public void setPlayerTypeId(Long playerTypeId) {
        this.playerTypeId = playerTypeId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}