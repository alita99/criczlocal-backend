package com.phoenix.criczlocal.db.entity;

import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Portal
 * 
 * @Author: sharoon
 */
@Entity
@Table(name = "PORTAL")
public class Portal extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PORTAL_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long portalId;

    @Column(name = "PORTAL_SIGNATURE")
    String portalSignature;

    @Column(name = "PORTAL_NAME")
    String portalName;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY")
    User createdBy;

    @Column(name = "IS_ACTIVE")
    Integer isActive;

    @Column(name = "IS_DELETE")
    Integer isDelete;

    public Long getPortalId() {
        return this.portalId;
    }

    public void setPortalId(Long portalId) {
        this.portalId = portalId;
    }

    public String getPortalSignature() {
        return this.portalSignature;
    }

    public void setPortalSignature(String portalSignature) {
        this.portalSignature = portalSignature;
    }

    public String getPortalName() {
        return this.portalName;
    }

    public void setPortalName(String portalName) {
        this.portalName = portalName;
    }

    public User getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}