package com.phoenix.criczlocal.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * TeamPlayer
 * 
 * @author Sharoon
 */
@Entity
@Table(name = "TEAM_PLAYER")
public class TeamPlayer extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "TEAM_PLAYER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long teamPlayerId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TEAM_ID", nullable = false)
    Team team;

    @ManyToOne
    @JoinColumn(name = "PLAYER_ID", nullable = false)
    Player player;

    @ManyToOne
    @JoinColumn(name = "PLAYER_TYPE_ID", nullable = false)
    PlayerType playerType;

    @Column(name = "IS_CAPTIAN")
    Integer isCaptian;

    @Column(name = "IS_VICE_CAPTIAN")
    Integer isViceCaptian;

    @ManyToOne
    @JoinColumn(name = "PORTAL_ID", nullable = false)
    Portal portal;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY", nullable = false)
    User createdBy;

    @Column(name = "IS_ACTIVE")
    Integer isActive;

    @Column(name = "IS_DELETE")
    Integer isDelete;

    public Long getTeamPlayerId() {
        return this.teamPlayerId;
    }

    public void setTeamPlayerId(Long teamPlayerId) {
        this.teamPlayerId = teamPlayerId;
    }

    public Team getTeam() {
        return this.team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Player getPlayer() {
        return this.player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public PlayerType getPlayerType() {
        return this.playerType;
    }

    public void setPlayerType(PlayerType playerType) {
        this.playerType = playerType;
    }

    public Integer getIsCaptian() {
        return this.isCaptian;
    }

    public void setIsCaptian(Integer isCaptian) {
        this.isCaptian = isCaptian;
    }

    public Integer getIsViceCaptian() {
        return this.isViceCaptian;
    }

    public void setIsViceCaptian(Integer isViceCaptian) {
        this.isViceCaptian = isViceCaptian;
    }

    public Portal getPortal() {
        return this.portal;
    }

    public void setPortal(Portal portal) {
        this.portal = portal;
    }

    public User getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}