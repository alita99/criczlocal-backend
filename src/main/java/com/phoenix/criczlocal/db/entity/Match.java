package com.phoenix.criczlocal.db.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Match
 * 
 * @Author: sharoon
 */
@Entity
@Table(name = "MATCHES")
public class Match extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "MATCH_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long matchId;

    @Column(name = "MATCH_NAME")
    String matchName;

    @Column(name = "START_ON")
    Date startOn;

    @ManyToOne
    @JoinColumn(name = "TEAM_ID1", nullable = true, updatable = false)
    Team team1;

    @ManyToOne
    @JoinColumn(name = "TEAM_ID2", nullable = true, updatable = false)
    Team team2;

    @ManyToOne
    @JoinColumn(name = "RULE_ID", nullable = true)
    PortalRule rule;

    @ManyToOne
    @JoinColumn(name = "PORTAL_ID", nullable = false)
    Portal portal;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY", nullable = false)
    User createdBy;

    @Column(name = "status")
    Integer status;

    @Column(name = "IS_ACTIVE")
    Integer isActive;

    @Column(name = "IS_DELETE")
    Integer isDelete;

    public Long getMatchId() {
        return this.matchId;
    }

    public void setMatchId(Long matchId) {
        this.matchId = matchId;
    }

    public String getMatchName() {
        return this.matchName;
    }

    public void setMatchName(String matchName) {
        this.matchName = matchName;
    }

    public Date getStartOn() {
        return this.startOn;
    }

    public void setStartOn(Date startOn) {
        this.startOn = startOn;
    }

    public Team getTeam1() {
        return this.team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return this.team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    public PortalRule getRule() {
        return this.rule;
    }

    public void setRule(PortalRule rule) {
        this.rule = rule;
    }

    public Portal getPortal() {
        return this.portal;
    }

    public void setPortal(Portal portal) {
        this.portal = portal;
    }

    public User getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }
}