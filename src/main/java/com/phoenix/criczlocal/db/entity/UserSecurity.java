package com.phoenix.criczlocal.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * UserSecurity
 * 
 * @Author: sharoon
 */
@Entity
@Table(name = "USER_SECURITY")
public class UserSecurity extends BaseEntity implements Cloneable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "SECURITY_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long securityId;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    User user;

    @Column(name = "SECURITY_KEY")
    String securityKey;

    @Column(name = "IS_ACTIVE")
    Integer isActive;

    public Long getSecurityId() {
        return this.securityId;
    }

    public void setSecurityId(Long securityId) {
        this.securityId = securityId;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSecurityKey() {
        return this.securityKey;
    }

    public void setSecurityKey(String securityKey) {
        this.securityKey = securityKey;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    @Override
    public UserSecurity clone() throws CloneNotSupportedException {
        return (UserSecurity) super.clone();
    }
}