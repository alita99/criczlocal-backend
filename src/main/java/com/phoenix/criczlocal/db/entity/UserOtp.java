package com.phoenix.criczlocal.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * UserOtp
 * 
 * @Author: sharoon
 */
@Entity
@Table(name = "USER_OTP")
public class UserOtp extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "OTP_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long otpId;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    User user;

    @Column(name = "OTP")
    String otp;

    @Column(name = "IS_VERIFIED")
    Integer isVerified;

    @Column(name = "IS_ACTIVE")
    Integer isActive;

    @Column(name = "IS_SMS_ENABLED")
    Integer isSmsEnabled;

    public Long getOtpId() {
        return this.otpId;
    }

    public void setOtpId(Long otpId) {
        this.otpId = otpId;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getOtp() {
        return this.otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public Integer getIsVerified() {
        return this.isVerified;
    }

    public void setIsVerified(Integer isVerified) {
        this.isVerified = isVerified;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getIsSmsEnabled() {
        return this.isSmsEnabled;
    }

    public void setIsSmsEnabled(Integer isSmsEnabled) {
        this.isSmsEnabled = isSmsEnabled;
    }
}