package com.phoenix.criczlocal.db.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Team
 * 
 * @Author: sharoon
 */
@Entity
@Table(name = "TEAM")
public class Team extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "TEAM_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long teamId;

    @Column(name = "SIGNATURE")
    String signature;

    @Column(name = "TEAM_NAME")
    String teamName;

    @Column(name = "TEAM_SHORT_NAME")
    String teamShortName;

    @ManyToOne
    @JoinColumn(name = "PORTAL_ID", nullable = false)
    Portal portal;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY", nullable = false)
    User createdBy;

    @Column(name = "IS_ACTIVE")
    Integer isActive;

    @Column(name = "IS_DELETE")
    Integer isDelete;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "TEAM_ID", updatable = false)
    List<TeamPlayer> teamPlayers;

    public Long getTeamId() {
        return this.teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }

    public String getSignature() {
        return this.signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getTeamName() {
        return this.teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getTeamShortName() {
        return this.teamShortName;
    }

    public void setTeamShortName(String teamShortName) {
        this.teamShortName = teamShortName;
    }

    public Portal getPortal() {
        return this.portal;
    }

    public void setPortal(Portal portal) {
        this.portal = portal;
    }

    public User getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public List<TeamPlayer> getTeamPlayers() {
        return this.teamPlayers;
    }

    public void setTeamPlayers(List<TeamPlayer> teamPlayers) {
        this.teamPlayers = teamPlayers;
    }
}