package com.phoenix.criczlocal.db.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Player
 * 
 * @Author: sharoon
 */
@Entity
@Table(name = "PLAYER")
public class Player extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PLAYER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long playerId;

    @Column(name = "SIGNATURE")
    String signature;

    // @ManyToOne(optional = true)
    // @JoinColumn(name = "USER_ID")
    @Transient
    User user;

    @Column(name = "NAME")
    String name;

    @Column(name = "EMAIL")
    String email;

    @Column(name = "MOBILE")
    String mobile;

    @ManyToOne
    @JoinColumn(name = "GENDER_ID")
    Gender gender;

    @Column(name = "DOB")
    Date dob;

    @ManyToOne
    @JoinColumn(name = "PLAYER_TYPE_ID")
    PlayerType playerType;

    @ManyToOne
    @JoinColumn(name = "PORTAL_ID", nullable = false)
    Portal portal;

    @ManyToOne
    @JoinColumn(name = "CREATED_BY", nullable = false)
    User createdBy;

    @Column(name = "IS_ACTIVE")
    Integer isActive;

    @Column(name = "IS_DELETE")
    Integer isDelete;

    public Long getPlayerId() {
        return this.playerId;
    }

    public void setPlayerId(Long playerId) {
        this.playerId = playerId;
    }

    public String getSignature() {
        return this.signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Gender getGender() {
        return this.gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return this.dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }
    
    public PlayerType getPlayerType() {
        return this.playerType;
    }

    public void setPlayerType(PlayerType playerType) {
        this.playerType = playerType;
    }

    public Portal getPortal() {
        return this.portal;
    }

    public void setPortal(Portal portal) {
        this.portal = portal;
    }

    public User getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getIsActive() {
        return this.isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Player playerId(Long playerId) {
        this.playerId = playerId;
        return this;
    }

    public Player signature(String signature) {
        this.signature = signature;
        return this;
    }

    public Player user(User user) {
        this.user = user;
        return this;
    }

    public Player name(String name) {
        this.name = name;
        return this;
    }

    public Player email(String email) {
        this.email = email;
        return this;
    }

    public Player mobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public Player playerType(PlayerType playerType) {
        this.playerType = playerType;
        return this;
    }

    public Player portal(Portal portal) {
        this.portal = portal;
        return this;
    }

    public Player isActive(Integer isActive) {
        this.isActive = isActive;
        return this;
    }

    public Player isDelete(Integer isDelete) {
        this.isDelete = isDelete;
        return this;
    }
}