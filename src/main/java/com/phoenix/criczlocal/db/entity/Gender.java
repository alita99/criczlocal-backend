package com.phoenix.criczlocal.db.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Gender
 * @Author: sharoon
 */
@Entity
@Table(name = "GENDER")
public class Gender extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "GENDER_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long genderId;

    @Column(name = "NAME")
    String name;

    @Column(name = "SHORT_NAME")
    String shortName;

    public Long getGenderId() {
        return this.genderId;
    }

    public void setGenderId(Long genderId) {
        this.genderId = genderId;
    }
   
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return this.shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}