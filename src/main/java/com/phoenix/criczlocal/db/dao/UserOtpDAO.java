package com.phoenix.criczlocal.db.dao;

import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.db.entity.User;
import com.phoenix.criczlocal.db.entity.UserOtp;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * UserOtpDAO
 * 
 * @Author: sharoon
 */
@Repository
public class UserOtpDAO extends BaseDAO<UserOtp> {

  UserOtpDAO() {
    super.setEntityClass(UserOtp.class);
  }

  /**
   * Get user otp.
   * 
   * @param user
   * @return
   */
  public UserOtp getUserOtp(User user) {
    Criteria crit = getCriteria();
    crit.add(Restrictions.eq("user", user));
    return (UserOtp) crit.uniqueResult();
  }

  /**
   * Get active users otp.
   * 
   * @param user
   * @return
   */
  public UserOtp getActiveUserOtp(User user) {
    Criteria crit = getCriteria();
    crit.add(Restrictions.eq("user", user));
    crit.add(Restrictions.eq("isActive", CommonConstants.ACTIVE));
    return (UserOtp) crit.uniqueResult();
  }
}