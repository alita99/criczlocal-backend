package com.phoenix.criczlocal.db.dao;

import java.util.List;

import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.db.entity.Portal;
import com.phoenix.criczlocal.db.entity.Team;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * TeamDAO
 * 
 * @Author: sharoon
 */
@Repository
public class TeamDAO extends BaseDAO<Team> {

    TeamDAO() {
        super.setEntityClass(Team.class);
    }

    /**
     * Get the team size of respective portals
     * 
     * @param portal
     * @return
     */
    public Long getTeamSize(Portal portal) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("portal", portal));
        criteria.add(Restrictions.eq("isDelete", CommonConstants.EXISIT));
        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();

        return count;
    }

    /**
     * Get the team from respective portal.
     * 
     * @param portal
     * @param page
     * @return
     */
    public List<Team> getTeams(Portal portal, Integer page) {

        Integer startResult = (page - 1) * Integer.parseInt(CommonConstants.LIST_PER_PAGE.toString());
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("portal", portal));
        criteria.add(Restrictions.eq("isDelete", CommonConstants.EXISIT));
        criteria.setFirstResult(startResult);
        criteria.setMaxResults(Integer.parseInt(CommonConstants.LIST_PER_PAGE.toString()));
        List<Team> teamList = (List<Team>) criteria.list();
        return teamList;
    }
}