package com.phoenix.criczlocal.db.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * CommonDAO
 */
@Repository
public class CommonDAO {

    @Autowired
    private SessionFactory sessionFactory;
    
    public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
    }
    
    private Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    public Criteria getCriteria(Class entityClass) {
        return getSession().createCriteria(entityClass);
    }

    @SuppressWarnings("uncheked")
    public <T> T get(Class<T> entityClass, Long id) {
        return (T) this.getSession().get(entityClass, id);
    }

    @SuppressWarnings("uncheked")
    public <T> List<T> getAll(Class<T> entityClass) {
        Criteria criteria = this.getSession().createCriteria(entityClass);
        return (List<T>) criteria.list();
    }

    public Serializable save(Object entity) {
        return this.getSession().save(entity);
    }

    public void update(Object entity) {
        this.getSession().update(entity);
    }

    public void saveOrUpdate(Object entity) {
        this.getSession().saveOrUpdate(entity);
    }

    public Object merge(Object entity) {
        return this.getSession().merge(entity);
    }

    public void delete(Object entity) {
        this.getSession().delete(entity);
    } 
}