package com.phoenix.criczlocal.db.dao;

import com.phoenix.criczlocal.db.entity.TeamPlayer;

import org.springframework.stereotype.Repository;

/**
 * TeamPlayerDAO
 * 
 * @Author: sharoon
 */
@Repository
public class TeamPlayerDAO extends BaseDAO<TeamPlayer> {

    TeamPlayerDAO() {
        super.setEntityClass(TeamPlayer.class);
    }
}