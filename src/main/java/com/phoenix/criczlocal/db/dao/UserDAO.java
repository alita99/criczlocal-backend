package com.phoenix.criczlocal.db.dao;

import java.util.List;

import com.phoenix.criczlocal.db.dao.BaseDAO;
import com.phoenix.criczlocal.db.dao.UserDAO;
import com.phoenix.criczlocal.db.entity.User;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * UserDAO
 * @Author: sharoon
 */
@Repository
public class UserDAO extends BaseDAO<User>{

	UserDAO() {
		super.setEntityClass(User.class);
	}

	/**
	 *Get the user by user name or mobile number.
	 * 
	 * @param userName
	 * @param mobileNo
	 * @return list of users.
	 */
	public List<User> getListUserByMobileNo(String mobileNo) {
		Criteria criteria = getCriteria();
		// Criterion userNameCriterion = Restrictions.eq("userName", userName);
		// Criterion mobileNoCriterion = Restrictions.eq("userMobile", mobileNo);
		// LogicalExpression orExp = Restrictions.or(userNameCriterion, mobileNoCriterion);
		criteria.add(Restrictions.eq("userMobile", mobileNo));
		return (List<User>) criteria.list();
	}

	/**
	 * Get the user by mobile number.
	 * 
	 * @param userMobileNo
	 * @return the user
	 */
	public User getUserByMobileNumber(String userMobileNo) {
		Criteria crit = getCriteria();
		crit.add(Restrictions.eq("userMobile", userMobileNo));
		return (User) crit.uniqueResult();
	}
}