package com.phoenix.criczlocal.db.dao;

import java.util.List;

import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.db.entity.Portal;
import com.phoenix.criczlocal.db.entity.PortalRule;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * StandardRulesDAO
 * 
 * @Author: sharoon
 */
@Repository
public class PortalRuleDAO extends BaseDAO<PortalRule> {

    PortalRuleDAO() {
        super.setEntityClass(PortalRule.class);
    }

    /**
     * Get the portal rules.
     * @return List<PortalRule>
     */
    public List<PortalRule> getPortalRules(Portal portal) {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("portal", portal));
        criteria.add(Restrictions.eq("isActive", CommonConstants.ACTIVE));
        List<PortalRule> PortalRuleList = (List<PortalRule>) criteria.list();
        return PortalRuleList;
    }
}