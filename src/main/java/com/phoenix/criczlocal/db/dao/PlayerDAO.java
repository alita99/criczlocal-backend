package com.phoenix.criczlocal.db.dao;

import java.util.List;

import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.db.entity.Player;
import com.phoenix.criczlocal.db.entity.Portal;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * PlayerDAO
 * 
 * @Author: sharoon
 */
@Repository
public class PlayerDAO extends BaseDAO<Player> {

    PlayerDAO() {
        super.setEntityClass(Player.class);
    }

    /**
     * Get the team size of respective portals
     * 
     * @param portal
     * @return
     */
    public Long getPlayerSize(Portal portal) {

        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("portal", portal));
        criteria.add(Restrictions.eq("isDelete", CommonConstants.EXISIT));
        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();

        return count;
    }

    /**
     * Get players of the particular portal.
     * 
     * @param portal
     * @return
     */
    public List<Player> getPlayers(Portal portal, Integer page) {

        Integer startResult = (page - 1) * Integer.parseInt(CommonConstants.LIST_PER_PAGE.toString());
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("portal", portal));
        criteria.add(Restrictions.eq("isDelete", CommonConstants.EXISIT));
        criteria.setFirstResult(startResult);
        criteria.setMaxResults(Integer.parseInt(CommonConstants.LIST_PER_PAGE.toString()));
        List<Player> playerList = (List<Player>) criteria.list();
        return playerList;
    }
}