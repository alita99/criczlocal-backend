package com.phoenix.criczlocal.db.dao;

import com.phoenix.criczlocal.db.entity.PortalUser;
import com.phoenix.criczlocal.db.entity.User;

import java.util.List;

import com.phoenix.criczlocal.db.entity.Portal;

import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * PortalUserDAO
 * @Author: sharoon
 */
@Repository
public class PortalUserDAO extends BaseDAO<PortalUser> {

    PortalUserDAO() {
        super.setEntityClass(PortalUser.class);
    }

    /**
     * Get user portals.
     * 
     * @param user
     * @return
     */
    public List<Portal> getUserPortals(User user) {
      Criteria criteria = getCriteria();
      criteria.add(Restrictions.eq("user", user));
      ProjectionList columns = Projections.projectionList().add(Projections.property("portal"));
      criteria.setProjection(columns);
      List<Portal> portalList = (List<Portal>) criteria.list();
      return portalList;
    }

    /**
     * Get portal users.
     * 
     * @param user
     * @return
     */
    public List<User> getPortalUsers(Portal portal) {
      Criteria criteria = getCriteria();
      criteria.add(Restrictions.eq("portal", portal));
      ProjectionList columns = Projections.projectionList().add(Projections.property("user"));
      criteria.setProjection(columns);
      List<User> userList = (List<User>) criteria.list();
      return userList;
    }

    /**
     * check portal users.
     * 
     * @param user
     * @return
     */
    public User checkUserPortal(Portal portal, User user) {
      Criteria criteria = getCriteria();
      criteria.add(Restrictions.eq("portal", portal));
      criteria.add(Restrictions.eq("user", user));
      ProjectionList columns = Projections.projectionList().add(Projections.property("user"));
      criteria.setProjection(columns);
      User checkUser = (User) criteria.uniqueResult();
      return checkUser;
    }
}