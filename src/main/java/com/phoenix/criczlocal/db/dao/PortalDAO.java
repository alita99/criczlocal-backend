package com.phoenix.criczlocal.db.dao;

import com.phoenix.criczlocal.db.dao.BaseDAO;
import com.phoenix.criczlocal.db.dao.PortalDAO;
import com.phoenix.criczlocal.db.entity.Portal;

import org.springframework.stereotype.Repository;

/**
 * PortalDAO
 * @Author: sharoon
 */
@Repository
public class PortalDAO extends BaseDAO<Portal> {

    PortalDAO() {
        super.setEntityClass(Portal.class);
    }
}