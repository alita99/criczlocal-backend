package com.phoenix.criczlocal.db.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * BaseDAO
 * @Author: sharoon
 */
@Repository
public class BaseDAO<T> {
    
    @Autowired
    private SessionFactory sessionFactory;

    private Class entityClass;

    /**
     * Set the entity class.
     * @param entityClass
     */
    public void setEntityClass(Class entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * Get the current hibernate session.
     * @return Session
     */
    private Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    /**
     * Get the criteria.
     * @return Criteria
     */
    public Criteria getCriteria() {
        return getSession().createCriteria(this.entityClass);
    }

    /**
     * Get the criteria based on entity class.
     * @param entityClass
     * @return Criteria
     */
    public Criteria getCriteria(Class entityClass) {
        return getSession().createCriteria(entityClass);
    }

    /**
     * Get the data from primary id.
     * @param <T>
     * @param id
     * @return
     */
    @SuppressWarnings("uncheked")
    public <T> T get(Long id) {
        return (T) this.getSession().get(this.entityClass, id);
    }

    /**
     * Get all data of the entity.
     * @param <T>
     * @return
     */
    @SuppressWarnings("uncheked")
    public <T> List<T> getAll() {
        Criteria criteria = this.getCriteria();
        return (List<T>) criteria.list();
    }

    /**
     * Save operation.
     * @param entity
     * @return
     */
    public T save(T entity) {
        return (T) this.getSession().save(entity);
    }

    /**
     * Update operation.
     * @param entity
     */
    public void update(T entity) {
        this.getSession().update(entity);
    }

    /**
     * Save or updat operation based on id.
     * @param entity
     */
    public void saveOrUpdate(Object entity) {
        this.getSession().saveOrUpdate(entity);
    }

    /**
     * Merge the object within the session.
     * @param entity
     * @return
     */
    public T merge(T entity) {
        return (T) this.getSession().merge(entity);
    }

    /**
     * Delete operation.
     * @param entity
     */
    public void delete(T entity) {
        this.getSession().delete(entity);
    } 

    /**
     * Delete list operation.
     * @param entities
     */
    public void delete(List<T> entities) {
        if (entities != null) {
            for (T entity : entities) {
                delete(entity);
            }
        }
    } 

    /**
     * Delete operation based on id.
     * @param id
     */
	public void delete(Long id) {
		T object = (T) get(id);
		if (null != object) {
			delete(object);
		}
    }
}