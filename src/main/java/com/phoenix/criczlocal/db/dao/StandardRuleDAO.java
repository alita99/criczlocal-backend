package com.phoenix.criczlocal.db.dao;

import java.util.List;

import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.db.entity.StandardRule;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * StandardRuleDAO
 * @Author: sharoon
 */
@Repository
public class StandardRuleDAO extends BaseDAO<StandardRule> {

    StandardRuleDAO() {
        super.setEntityClass(StandardRule.class);
    }

    /**
     * Get the active standard rules.
     * @return List<StandardRule>
     */
    public List<StandardRule> getActiveStandardRule() {
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("isActive", CommonConstants.ACTIVE));
        List<StandardRule> StandardRuleList = (List<StandardRule>) criteria.list();
        return StandardRuleList;
    }
}