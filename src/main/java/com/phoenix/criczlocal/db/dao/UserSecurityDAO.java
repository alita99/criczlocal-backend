package com.phoenix.criczlocal.db.dao;

import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.db.entity.User;
import com.phoenix.criczlocal.db.entity.UserSecurity;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * UserSecurityDAO
 * @Author: sharoon
 */
@Repository
public class UserSecurityDAO extends BaseDAO<UserSecurity> {

    UserSecurityDAO() {
        super.setEntityClass(UserSecurity.class);
    }

	public UserSecurity getActiveUserSecurity(User user) {
		Criteria crit = getCriteria();
		crit.add(Restrictions.eq("user", user));
		crit.add(Restrictions.eq("isActive", CommonConstants.ACTIVE));
		return (UserSecurity) crit.uniqueResult();
	}
}