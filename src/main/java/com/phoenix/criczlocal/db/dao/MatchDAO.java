package com.phoenix.criczlocal.db.dao;

import java.util.List;

import com.phoenix.criczlocal.constant.CommonConstants;
import com.phoenix.criczlocal.db.entity.Match;
import com.phoenix.criczlocal.db.entity.Portal;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * MatchDAO
 * 
 * @Author: sharoon
 */
@Repository
public class MatchDAO extends BaseDAO<Match> {

    MatchDAO() {
        super.setEntityClass(Match.class);
    }

    /**
     * Get match total size.
     * 
     * @param portal
     * @return
     */
    public Long getMatchSize(Portal portal) {

        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("portal", portal));
        criteria.add(Restrictions.eq("isDelete", CommonConstants.EXISIT));
        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();

        return count;
    }

    /**
     * Get the portal matches.
     * 
     * @param portal
     * @param page
     * @return
     */
    public List<Match> getPortalMatches(Portal portal, Integer page) {

        Integer startResult = (page - 1) * Integer.parseInt(CommonConstants.LIST_PER_PAGE.toString());
        Criteria criteria = getCriteria();
        criteria.add(Restrictions.eq("portal", portal));
        criteria.add(Restrictions.eq("isDelete", CommonConstants.EXISIT));
        criteria.addOrder(Order.asc("startOn"));
        criteria.setFirstResult(startResult);
        criteria.setMaxResults(Integer.parseInt(CommonConstants.LIST_PER_PAGE.toString()));
        List<Match> matchList = (List<Match>) criteria.list();
        return matchList;
    }
}