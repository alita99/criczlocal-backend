package com.phoenix.criczlocal.db.exception;

/**
 * RecordNotFoundException
 * @Author: sharoon
 */
public class RecordNotFoundException extends Exception{

    public RecordNotFoundException(String message) {
        super(message);
    }
}