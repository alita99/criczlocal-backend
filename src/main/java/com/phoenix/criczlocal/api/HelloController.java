package com.phoenix.criczlocal.api;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * HelloController
 * @Author: sharoon
 */
@RestController
@CrossOrigin
public class HelloController extends BaseController {

	/**
	 * Get greeting to test authentication works.
	 * @return ResponseEntity<?>
	 */
    @RequestMapping("/greetings")
	public ResponseEntity<?> index() {
		try {
			String greetings = "Greetings from CricZLocal";
			return returnResponse(greetings);
		} catch (Exception e) {
			return returnException(e);
		}
	}
}