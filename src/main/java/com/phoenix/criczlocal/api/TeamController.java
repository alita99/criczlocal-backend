package com.phoenix.criczlocal.api;

import java.util.List;
import java.util.Map;

import com.phoenix.criczlocal.action.TeamAction;
import com.phoenix.criczlocal.constant.WebConstants;
import com.phoenix.criczlocal.model.TeamDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * TeamController
 * 
 * @Author: sharoon
 */
@RestController
@CrossOrigin
@RequestMapping("/portal")
public class TeamController extends BaseController {

    @Autowired
    TeamAction teamAction;

    /**
     * Save the team.
     * 
     * @param portalId
     * @param teamDTO
     * @return
     */
    @PostMapping("/{portalId}/team")
    public ResponseEntity<?> saveTeam(@PathVariable("portalId") Long portalId, @RequestBody TeamDTO teamDTO) {
        try {
            teamDTO = teamAction.save(teamDTO, portalId, getRequestUser());
            return returnResponse(WebConstants.TEAM_DETAILS, teamDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return returnException(e);
        }
    }

    /**
     * Get the teams of the respective portal.
     * 
     * @param portalId
     * @param page
     * @return
     */
    @GetMapping("/{portalId}/team")
    public ResponseEntity<?> getTeams(@PathVariable("portalId") Long portalId,
            @RequestParam(name = "page", required = false) Integer page) {
        try {
            page = (page == null) ? new Integer(1) : page;
            Map<String, Object> teamDetails = teamAction.getTeams(portalId, page);
            return returnResponse(teamDetails);
        } catch (Exception e) {
            e.printStackTrace();
            return returnException(e);
        }
    }

    /**
     * Get the respective team.
     * 
     * @param portalId
     * @param teamId
     * @return
     */
    @GetMapping("/{portalId}/team/{teamId}")
    public ResponseEntity<?> getTeam(@PathVariable("portalId") Long portalId, @PathVariable("teamId") Long teamId) {
        try {
            TeamDTO teamDTO = teamAction.getTeam(teamId, portalId);
            return returnResponse(WebConstants.TEAM_DETAILS, teamDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return returnException(e);
        }
    }

    /**
     * Update the respective team.
     * 
     * @param portalId
     * @param teamId
     * @param teamDTO
     * @return
     */
    @PutMapping("/{portalId}/team/{teamId}")
    public ResponseEntity<?> updateTeam(@PathVariable("portalId") Long portalId, @PathVariable("teamId") Long teamId,
            @RequestBody TeamDTO teamDTO) {
        try {
            teamDTO = teamAction.updateTeam(teamDTO, portalId, getRequestUser());
            return returnResponse(WebConstants.TEAM_DETAILS, teamDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return returnException(e);
        }
    }

    /**
     * Delete the respective team.
     * 
     * @param portalId
     * @param teamId
     * @return
     */
    @DeleteMapping("/{portalId}/team/{teamId}")
    public ResponseEntity<?> deleteTeam(@PathVariable("portalId") Long portalId, @PathVariable("teamId") Long teamId) {
        try {
            teamAction.deleteTeam(teamId, portalId);
            return returnResponse("Success");
        } catch (Exception e) {
            e.printStackTrace();
            return returnException(e);
        }
    }

    /**
     * Delete the multiple team.
     * 
     * @param portalId
     * @param teamIds
     * @return
     */
    @DeleteMapping("/{portalId}/team")
    public ResponseEntity<?> deleteTeam(@PathVariable("portalId") Long portalId, @RequestBody List<Long> teamIds) {
        try {
            teamAction.deleteTeam(teamIds, portalId);
            return returnResponse("Success");
        } catch (Exception e) {
            e.printStackTrace();
            return returnException(e);
        }
    }
}