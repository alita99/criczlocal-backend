package com.phoenix.criczlocal.api;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import com.phoenix.criczlocal.action.UserAction;
import com.phoenix.criczlocal.base.BeanConversionBase;
import com.phoenix.criczlocal.constant.ResponseConstant;
import com.phoenix.criczlocal.constant.ResponseStatusCode;
import com.phoenix.criczlocal.constant.WebConstants;
import com.phoenix.criczlocal.model.web.ErrorResponseBody;
import com.phoenix.criczlocal.model.web.ResponseBody;
import com.phoenix.criczlocal.db.entity.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * BaseController
 * 
 * @Author: sharoon
 */
@CrossOrigin
public class BaseController extends BeanConversionBase {

    @Autowired
    UserAction userAction;

    @Autowired
    Validator validator;

    /**
     * Prepare the success response data.
     * 
     * @param data
     * @return ResponseBody
     */
    private ResponseBody prepareResponse(Object data) {
        ResponseBody reponseBody = new ResponseBody();
        reponseBody.setIsSuccess(ResponseConstant.SUCCESS);
        reponseBody.setStatusCode(ResponseStatusCode.SUCCESS);
        reponseBody.setData(data);
        return reponseBody;
    }

    /**
     * Prepare the success response data with key value pair.
     * 
     * @param key
     * @param data
     * @return ResponseBody
     */
    private ResponseBody prepareResponse(Object key, Object data) {
        ResponseBody reponseBody = new ResponseBody();
        HashMap returnData = new HashMap();
        returnData.put(key, data);
        reponseBody.setData(returnData);
        reponseBody.setStatusCode(ResponseStatusCode.SUCCESS);
        reponseBody.setIsSuccess(ResponseConstant.SUCCESS);
        return reponseBody;
    }

    /**
     * Prepare the fail response with key value pair.
     * 
     * @param key
     * @param validationData
     * @return ResponseBody
     */
    private ResponseBody prepareValidation(String title, String message, Object data) {
        ResponseBody reponseBody = new ResponseBody();
        reponseBody.setStatusCode(ResponseStatusCode.CONFLICT);
        reponseBody.setIsSuccess(ResponseConstant.FAIL);

        ErrorResponseBody error = new ErrorResponseBody();
        error.setTitle(title);
        error.setMessage(message);
        error.setErrorData(data);
        reponseBody.setError(error);

        return reponseBody;
    }

    /**
     * Prepare the fail response with data.
     * 
     * @param data
     * @return ResponseBody
     */
    private ResponseBody prepareException(String title) {
        ResponseBody reponseBody = new ResponseBody();
        reponseBody.setIsSuccess(ResponseConstant.FAIL);
        reponseBody.setStatusCode(ResponseStatusCode.INTERNAL_SERVER_ERROR);

        ErrorResponseBody error = new ErrorResponseBody();
        error.setTitle(title);
        reponseBody.setError(error);

        return reponseBody;
    }

    /**
     * Return empty success response.
     * 
     * @return ResponseEntity<?>
     */
    public ResponseEntity<?> returnResponse() {
        return ResponseEntity.noContent().build();
    }

    /**
     * Return the response data.
     * 
     * @param data
     * @return ResponseEntity<?>
     */
    public ResponseEntity<?> returnResponse(Object data) {
        return ResponseEntity.ok(prepareResponse(data));
    }

    /**
     * Return the response data with key value pair.
     * 
     * @param key
     * @param data
     * @return ResponseEntity<?>
     */
    public ResponseEntity<?> returnResponse(Object key, Object data) {
        if (key == null) {
            return returnResponse(data);
        }
        return ResponseEntity.ok(prepareResponse(key, data));
    }

    /**
     * Return the validation message.
     * 
     * @param title
     * @return ResponseEntity<?>
     */
    public ResponseEntity<?> returnValidation(String title) {
        return returnValidation(title, null, null);
    }

    /**
     * Return the validation message.
     * 
     * @param title
     * @param message
     * @return ResponseEntity<?>
     */
    public ResponseEntity<?> returnValidation(String title, String message) {
        return returnValidation(title, message, null);
    }

    /**
     * Return the validation message.
     * 
     * @param title
     * @param message
     * @param errorData
     * @return
     */
    public ResponseEntity<?> returnValidation(String title, String message, Object errorData) {
        return ResponseEntity.status(HttpStatus.CONFLICT).body(prepareValidation(title, message, errorData));
    }

    /**
     * Return the exception details.
     * 
     * @param e the exception
     * @return ResponseEntity<?>
     */
    public ResponseEntity<?> returnException(Exception e) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(prepareException(e.getMessage()));
    }

    /**
     * Return the exception details.
     * 
     * @param e the exception
     * @return ResponseEntity<?>
     */
    public ResponseEntity<?> returnException() {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(prepareException(WebConstants.INTERNAL_SERVER_ERROR));
    }

    /**
     * Get the logined user id using token.
     * 
     * @return user id
     */
    public Long getRequestUserId() {
        Long userId = null;
        org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder
                .getContext().getAuthentication().getPrincipal();

        if (user.getUsername() != null) {
            userId = Long.parseLong(user.getUsername());
        }
        return userId;
    }

    /**
     * Get the logined user using token.
     * 
     * @return User
     */
    public User getRequestUser() {
        return userAction.getUser(getRequestUserId());
    }

    /**
     * Validate the model.
     * 
     * @param model
     * @return map
     */
    public <T> Map<String, String> validateModel(T model) {

        try {
            if (model == null) {
                return null;
            }
            Map<String, String> validationMessage = new HashMap<String, String>();
            Set<ConstraintViolation<T>> violations = validator.validate(model);
            for (ConstraintViolation<T> violation : violations) {
                String propertyPath = violation.getPropertyPath().toString();
                String message = violation.getMessage();

                validationMessage.put(propertyPath, message);
            }
            return validationMessage;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}