package com.phoenix.criczlocal.api;

import java.util.List;

import com.phoenix.criczlocal.action.PortalAction;
import com.phoenix.criczlocal.constant.WebConstants;
import com.phoenix.criczlocal.model.PortalDTO;
import com.phoenix.criczlocal.model.PortalRuleDTO;
import com.phoenix.criczlocal.model.UserDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * PortalController
 * 
 * @Author: sharoon
 */
@RestController
@CrossOrigin
@RequestMapping("/portal")
public class PortalController extends BaseController {

    @Autowired
    PortalAction portalAction;

    /**
     * Get portal users.
     * 
     * @param portalId
     * @return
     */
    @GetMapping("/{portalId}/users")
    public ResponseEntity<?> getPortalUsers(@PathVariable("portalId") Long portalId) {
        try {
            List<UserDTO> userDTOs = portalAction.getPortalUsers(portalId);
            return returnResponse(WebConstants.USER_DETAILS, userDTOs);
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * Get portal details by portalId
     * 
     * @param portalId
     * @return
     */
    @GetMapping("/{portalId}")
    public ResponseEntity<?> getPortal(@PathVariable("portalId") Long portalId) {
        try {
            PortalDTO portalDTO = portalAction.getPortal(portalId);
            return returnResponse(WebConstants.PORTAL_DETAILS, portalDTO);
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * Update portal details by portalId
     * 
     * @param portalId
     * @return
     */
    @PutMapping("/{portalId}")
    public ResponseEntity<?> updatePortal(@PathVariable("portalId") Long portalId, @RequestBody PortalDTO portalDTO) {
        try {
            portalDTO = portalAction.updatePortal(portalId, portalDTO);
            return returnResponse(WebConstants.PORTAL_DETAILS, portalDTO);
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * Update portal details by portalId
     * 
     * @param portalId
     * @return
     */
    @DeleteMapping("/{portalId}")
    public ResponseEntity<?> deletePortal(@PathVariable("portalId") Long portalId) {
        try {
            portalAction.deletePortal(portalId);
            return returnResponse();
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * Get the portal rules.
     * 
     * @param portalId
     * @return
     */
    @GetMapping("/{portalId}/rules")
    public ResponseEntity<?> getPortalRules(@PathVariable("portalId") Long portalId) {
        try {
            List<PortalRuleDTO> portalRuleDTOs = portalAction.getPortalRules(portalId);
            return returnResponse(WebConstants.RULES, portalRuleDTOs);
        } catch (Exception e) {
            return returnException(e);
        }
    }
}