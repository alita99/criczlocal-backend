package com.phoenix.criczlocal.api;

import java.util.List;

import com.phoenix.criczlocal.action.UserAction;
import com.phoenix.criczlocal.db.entity.User;
import com.phoenix.criczlocal.db.exception.RecordNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * UserController
 * @Author: sharoon
 */
@RestController
@CrossOrigin
@RequestMapping("/users")
public class UserController extends BaseController {

    @Autowired
    UserAction userAction;

    @GetMapping
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> userList = userAction.getAllUsers();

        return new ResponseEntity<List<User>>(userList, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable("id") Long userId) throws RecordNotFoundException {
        User user = userAction.getUser(userId);
        return new ResponseEntity<User>(user, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<User> addUser(@RequestBody User user) throws RecordNotFoundException {
        User addedUser = userAction.saveUser(user);
        return new ResponseEntity<User>(addedUser, new HttpHeaders(), HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<User> updateUser(@RequestBody User user) throws RecordNotFoundException {
        User updateUser = userAction.updateUser(user);
        return new ResponseEntity<User>(updateUser, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<User> deleteUser(@PathVariable("id") Long userId) throws RecordNotFoundException {
        userAction.deleteUser(userId);
        return new ResponseEntity<User>(new HttpHeaders(), HttpStatus.OK);
    }
}