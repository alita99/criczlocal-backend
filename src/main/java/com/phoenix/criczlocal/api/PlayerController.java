package com.phoenix.criczlocal.api;

import java.util.List;
import java.util.Map;

import com.phoenix.criczlocal.action.PlayerAction;
import com.phoenix.criczlocal.constant.ResponseConstant;
import com.phoenix.criczlocal.constant.WebConstants;
import com.phoenix.criczlocal.model.PlayerDTO;
import com.phoenix.criczlocal.model.PlayerTypeDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * PlayerController
 * 
 * @Author: sharoon
 */
@RestController
@CrossOrigin
@RequestMapping("/portal")
public class PlayerController extends BaseController {

    @Autowired
    PlayerAction playerAction;

    /**
     * Save player for portal.
     * 
     * @param portalId
     * @param playerDTO
     * @return
     */
    @PostMapping("/{portalId}/player")
    public ResponseEntity<?> savePlayer(@PathVariable("portalId") Long portalId, @RequestBody PlayerDTO playerDTO) {
        try {
            playerDTO = playerAction.savePlayer(playerDTO, portalId, getRequestUserId());
            return returnResponse(WebConstants.PLAYER_DETAILS, playerDTO);
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * Get all the players associated with portal.
     * 
     * @param portalId
     * @return
     */
    @GetMapping("/{portalId}/player")
    public ResponseEntity<?> getPlayers(@PathVariable("portalId") Long portalId, @RequestParam(name = "page", required = false) Integer page) {
        try {
            page = (page == null) ? new Integer(1) : page;
            Map<String, Object> playerDetails = playerAction.getPlayers(portalId, page);
            return returnResponse(playerDetails);
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * Get the player.
     * 
     * @param portalId
     * @return
     */
    @GetMapping("/{portalId}/player/{playerId}")
    public ResponseEntity<?> getPlayer(@PathVariable("portalId") Long portalId,
            @PathVariable("playerId") Long playerId) {
        try {
            PlayerDTO playerDTO = playerAction.getPlayer(playerId, portalId);
            return returnResponse(WebConstants.PLAYER_DETAILS, playerDTO);
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * Update the player
     * 
     * @param portalId
     * @param playerId
     * @param playerDTO
     * @return
     */
    @PutMapping("/{portalId}/player/{playerId}")
    public ResponseEntity<?> updatePlayer(@PathVariable("portalId") Long portalId,
            @PathVariable("playerId") Long playerId, @RequestBody PlayerDTO playerDTO) {
        try {
            playerDTO = playerAction.updatePlayer(playerDTO, portalId);
            return returnResponse(WebConstants.PLAYER_DETAILS, playerDTO);
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * Delete the player.
     * 
     * @param portalId
     * @param playerId
     * @return
     */
    @DeleteMapping("/{portalId}/player/{playerId}")
    public ResponseEntity<?> deletePlayer(@PathVariable("portalId") Long portalId,
            @PathVariable("playerId") Long playerId) {
        try {
            playerAction.deletePlayer(playerId, portalId);
            return returnResponse(ResponseConstant.DONE);
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * Delete the players.
     * 
     * @param portalIds
     * @param playerId
     * @return
     */
    @DeleteMapping("/{portalId}/player")
    public ResponseEntity<?> deletePlayer(@PathVariable("portalId") Long portalId, @RequestBody List<Long> playerIds) {
        try {
            playerAction.deletePlayer(playerIds, portalId);
            return returnResponse(ResponseConstant.DONE);
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * Get the player types
     * 
     * @param portalId
     * @return
     */
    @GetMapping("{portalId}/getAllPlayerTypes")
    public ResponseEntity<?> getPlayerType(@PathVariable("portalId") Long portalId) {
        try {
            List<PlayerTypeDTO> playerTypes = playerAction.getPlayerTypes(portalId);
            return returnResponse(WebConstants.PLAYER_TYPES, playerTypes);
        } catch (Exception e) {
            return returnException(e);
        }
    }
}