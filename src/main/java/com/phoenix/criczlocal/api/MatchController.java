package com.phoenix.criczlocal.api;

import java.util.List;
import java.util.Map;

import com.phoenix.criczlocal.action.MatchAction;
import com.phoenix.criczlocal.constant.WebConstants;
import com.phoenix.criczlocal.model.MatchDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/portal")
public class MatchController extends BaseController {

    @Autowired
    MatchAction matchAction;

    /**
     * Save the match.
     * 
     * @param portalId
     * @param matchDTO
     * @return
     */
    @PostMapping("/{portalId}/match")
    public ResponseEntity<?> saveMatch(@PathVariable("portalId") Long portalId, @RequestBody MatchDTO matchDTO) {
        try {
            matchDTO = matchAction.saveMatch(matchDTO, portalId, getRequestUserId());
            return returnResponse(WebConstants.MATCH_DETAILS, matchDTO);
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * update the match details.
     * 
     * @param portalId
     * @param matchId
     * @param matchDTO
     * @return
     */
    @PutMapping("/{portalId}/match/{matchId}")
    public ResponseEntity<?> updateMatch(@PathVariable("portalId") Long portalId, @PathVariable("matchId") Long matchId,
            @RequestBody MatchDTO matchDTO) {
        try {
            matchDTO = matchAction.updateMatch(matchDTO, matchId, portalId, getRequestUserId());
            return returnResponse(WebConstants.MATCH_DETAILS, matchDTO);
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * Get the match.
     * 
     * @param portalId
     * @param matchId
     * @return
     */
    @GetMapping("/{portalId}/match/{matchId}")
    public ResponseEntity<?> getMatch(@PathVariable("portalId") Long portalId, @PathVariable("matchId") Long matchId) {
        try {
            MatchDTO matchDTO = matchAction.getMatch(matchId, portalId);
            return returnResponse(WebConstants.MATCH_DETAILS, matchDTO);
        } catch (Exception e) {
            e.printStackTrace();
            return returnException(e);
        }
    }

    /**
     * Delete respective match.
     * 
     * @param portalId
     * @param matchId
     * @return
     */
    @DeleteMapping("/{portalId}/match/{matchId}")
    public ResponseEntity<?> deleteMatch(@PathVariable("portalId") Long portalId,
            @PathVariable("matchId") Long matchId) {
        try {
            matchAction.deleteMatch(matchId, portalId);
            return returnResponse("Success");
        } catch (Exception e) {
            e.printStackTrace();
            return returnException(e);
        }
    }

    /**
     * Delete multiple matches.
     * 
     * @param portalId
     * @param matchIds
     * @return
     */
    @DeleteMapping("/{portalId}/match")
    public ResponseEntity<?> deleteMatch(@PathVariable("portalId") Long portalId, @RequestBody List<Long> matchIds) {
        try {
            matchAction.deleteMatch(matchIds, portalId);
            return returnResponse("Success");
        } catch (Exception e) {
            e.printStackTrace();
            return returnException(e);
        }
    }

    /**
     * Get the portal matches.
     * 
     * @param portalId
     * @param page
     * @return
     */
    @GetMapping("/{portalId}/match")
    public ResponseEntity<?> deleteMatch(@PathVariable("portalId") Long portalId,
            @RequestParam(name = "page", required = false) Integer page) {
        try {
            page = (page == null) ? new Integer(1) : page;
            Map<String, Object> matchDetails = matchAction.getPortalMatches(portalId, page);
            return returnResponse(matchDetails);
        } catch (Exception e) {
            return returnException(e);
        }
    }
}