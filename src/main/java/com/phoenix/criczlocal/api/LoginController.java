package com.phoenix.criczlocal.api;

import java.util.List;
import java.util.Map;

import javax.xml.bind.ValidationException;

import com.phoenix.criczlocal.action.OnBoardAction;
import com.phoenix.criczlocal.action.UserAction;
import com.phoenix.criczlocal.constant.ErrorDetails;
import com.phoenix.criczlocal.constant.WebConstants;
import com.phoenix.criczlocal.db.entity.User;
import com.phoenix.criczlocal.db.exception.RecordNotFoundException;
import com.phoenix.criczlocal.model.PortalDTO;
import com.phoenix.criczlocal.model.auth.OnBoardUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * LoginController
 * 
 * @Author: sharoon
 */
@RestController
@CrossOrigin
public class LoginController extends BaseController {

    @Autowired
    OnBoardAction onBoardAction;

    @Autowired
    UserAction userAction;

    /**
     * Register user details.
     * 
     * @param onBoardUser
     * @return
     */
    @PostMapping("/onBoard")
    public ResponseEntity<?> onBoardUser(@RequestBody OnBoardUser onBoardUser) {
        try {

            Map<String, String> validationMessage = validateModel(onBoardUser);
            if (validationMessage != null && !validationMessage.isEmpty()) {
                return returnValidation(ErrorDetails.REGISTER_ERROR_MESSAGE, null, validationMessage);
            }

            List<User> exsistingUsers = userAction.getListUserByMobileNo(onBoardUser.getMobileNo());
            if (exsistingUsers.size() > 0) {
                return returnValidation(ErrorDetails.MOBILE_NUMBER_ALREADY_REGISTER, null, null);
            }

            User user = onBoardAction.registerUser(onBoardUser);
            return returnResponse(onBoardAction.prepareJwtResponse(user));
        } catch (ValidationException e) {
            return returnValidation(e.getMessage());
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * Veriy the respective user by mobile number.
     * 
     * @param mobileNumber
     * @param otp
     * @return
     */
    @GetMapping("/verifyUser")
    public ResponseEntity<?> verifyUser(@RequestParam(name = "userId") Long userId,
            @RequestParam(name = "otp") String otp) {
        try {

            User user = onBoardAction.verifyUser(userId, otp);
            return returnResponse(onBoardAction.prepareJwtResponse(user));
        } catch (RecordNotFoundException e) {
            e.printStackTrace();
            return returnValidation(e.getMessage(), "Please register.");
        } catch (ValidationException e) {
            e.printStackTrace();
            return returnException(e);
        }
    }

    /**
     * Get the logined user.
     * 
     * @return the user details.
     */
    @GetMapping("/loginedUser")
    public ResponseEntity<?> getLoginedUser() {
        try {
            User user = getRequestUser();
            return returnResponse(WebConstants.USER_DETAILS, user);
        } catch (Exception e) {
            return returnException(e);
        }
    }

    /**
     * Get the logined user portal details.
     * 
     * @return
     */
    @GetMapping("/portals")
    public ResponseEntity<?> getUserPortals() throws RecordNotFoundException {
        List<PortalDTO> portals = userAction.getUserPortals(getRequestUser());
        return returnResponse(WebConstants.PORTAL_DETAILS, portals);
    }

    /**
     * re Send the otp for the respective user.
     * 
     * @return
     */
    @GetMapping("/reSendUserOtp")
    public ResponseEntity<?> reSendUserOtp(@RequestParam(name = "userId") Long userId) {
        try {
            onBoardAction.reSendUserOtp(userId);
            return returnResponse("Success");
        } catch (RecordNotFoundException e) {
            return returnException(e);
        }
    }

    /**
     * re Send the otp for the respective mobile number.
     * 
     * @return
     */
    @GetMapping("/reSendOtp")
    public ResponseEntity<?> reSendOtp(@RequestParam(name = "mobileNumber") String mobileNumber) {
        try {
            onBoardAction.resendOtp(mobileNumber);
            return returnResponse("Success");
        } catch (RecordNotFoundException e) {
            return returnException(e);
        }
    }

    /**
     * Send the otp for the respective mobile number.
     * 
     * @return
     */
    @GetMapping("/sendOtp")
    public ResponseEntity<?> sendOtp(@RequestParam(name = "mobileNumber") String mobileNumber) {
        try {
            onBoardAction.sendOtp(mobileNumber);
            return returnResponse("Success");
        } catch (RecordNotFoundException e) {
            return returnException(e);
        }
    }

    /**
     * Send the otp for the respective mobile number.
     * 
     * @return
     */
    @GetMapping("/checkOtp")
    public ResponseEntity<?> verifyOtp(@RequestParam(name = "mobileNumber") String mobileNumber,
            @RequestParam(name = "otp") String otp) {
        try {
            onBoardAction.verifyOtp(mobileNumber, otp);
            return returnResponse("Success");
        } catch (RecordNotFoundException | ValidationException e) {
            return returnException(e);
        }
    }

    /**
     * Send the otp for the respective mobile number.
     * 
     * @return
     * @throws CloneNotSupportedException
     */
    @GetMapping("/resetSecurity")
    public ResponseEntity<?> resetSecurity(@RequestParam(name = "mobileNumber") String mobileNumber,
            @RequestParam(name = "secretKey") String secretKey) {
        try {
            onBoardAction.resetSecurity(mobileNumber, secretKey);
            return returnResponse("Success");
        } catch (RecordNotFoundException | ValidationException | CloneNotSupportedException e) {
            return returnException(e);
        }
    }
}