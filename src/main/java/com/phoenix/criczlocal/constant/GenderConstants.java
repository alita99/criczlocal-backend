package com.phoenix.criczlocal.constant;

public enum GenderConstants {

    MALE(1L),

    FEMALE(2L);

    private final Long value;
  
    GenderConstants(Long value) {
        this.value = value;
    }

    public Long getValue(){
      return this.value;
    }
}