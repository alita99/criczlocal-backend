package com.phoenix.criczlocal.constant;

/**
 * RoleConstants
 */
public enum RoleConstants {
    SUPER_ADMIN(99L),

    ADMIN(1L),

    USER(11L),

    GUEST(111L);

    private final Long value;
  
    RoleConstants(Long value) {
        this.value = value;
    }

    public Long getValue(){
      return this.value;
    }
}