package com.phoenix.criczlocal.constant;

public class WebConstants {

    public static final String EXCEPTION_MESSAGE = "ExceptionDetails";

    public static final String VALIDATION_DETAILS = "ValidationDetails";

    public static final String INTERNAL_SERVER_ERROR = "Inernal server error";

    public static final String CURRENT_PAGE = "currentPage";

    public static final String TOTAL_PAGE = "totalPage";

    public static final String USER_DETAILS = "userDetails";

    public static final String PORTAL_DETAILS = "portalDetails";

    public static final String PLAYER_DETAILS = "playerDetails";

    public static final String PLAYER_TYPES = "playerTypes";

    public static final String TEAM_DETAILS = "teamDetails";

    public static final String MATCH_DETAILS = "matchDetails";

    public static final String RULES = "rules";
}