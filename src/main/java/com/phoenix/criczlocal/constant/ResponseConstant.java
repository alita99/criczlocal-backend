package com.phoenix.criczlocal.constant;

public class ResponseConstant {

    public final static Boolean SUCCESS = Boolean.TRUE;

    public final static Boolean FAIL = Boolean.FALSE;

    public static final String DONE = "DONE";
}