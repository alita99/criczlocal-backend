package com.phoenix.criczlocal.constant;

/**
 * ResponseStatusCode
 * @Author: sharoon
 */
public class ResponseStatusCode {

    public static Integer SUCCESS = 200;

    public static Integer NON_AUTHORITATIVE = 203;
    
    public static Integer NOT_FOUND = 404;

    public static Integer CONFLICT = 409;

    public static Integer INTERNAL_SERVER_ERROR = 500;
}