package com.phoenix.criczlocal.constant;

public class CommonConstants {

    public static final Integer IN_ACTIVE = 0;

    public static final Integer ACTIVE = 1;

    public static final Integer ENDED = 2;

    public static final Integer PUSED = 3;

    public static final Integer DELETE = 1;

    public static final Integer EXISIT = 0;

    public static final Long LIST_PER_PAGE = 11L;
}